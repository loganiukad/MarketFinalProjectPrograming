<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <title></title>
</head>

<body>
<table align="right">
    <tbody>
    <form name="Hello" action="/">
        <style>
            table, th, td {
                border: 5px solid crimson;
            }
        </style>
        <meta charset="UTF-8">
        <title></title>
        <tr>
            <th>
                Hello
                <c:out value="${sessionScope.user.firstName} ${sessionScope.user.secondName}"></c:out> from home page.
            </th>
        <tr>
        <tr>
    </form>
    <tr>
    </tr>
    </tbody>
</table>


<table align="right">
    <tbody>
    <form name="logOut" method="post" action="/logOut">
        <style>
            table, th, td {
                border: 5px solid crimson;
            }
        </style>
        <meta charset="UTF-8">
        <title></title>
        <tr>
            <th>
                <input type="submit" value="logOut"/>
            </th>
        <tr>
        <tr>
    </form>
    <tr>
    </tr>
    </tbody>
</table>


<table align="right">
    <tbody>
    <form name="Hello" action="/basket">
        <style>
            table, th, td {
                border: 5px solid crimson;
            }
        </style>
        <meta charset="UTF-8">
        <title></title>
        <tr>
        <tr>
            <th><input type="submit" value="Basket"/></th>
        </tr>
        </th>
        <tr>
        <tr>

    </form>
    </tbody>
</table>

<table align="left">
    <tbody>
    <form name="findProductByProductGroups" method="post" action="/homePage">
        <style>
            table, th, td {
                border: 5px solid crimson;
            }
        </style>
        <meta charset="UTF-8">
        <title></title>
        <tr>
            <th> Catalog: <br/></th>
        </tr>
        <tr>
            <c:forEach items="${requestScope.productGroups}" var="groups">
        <tr>
            <form name="findProductByGroups" method="post" action="/homePage">
                <th><input type="submit" value="${groups.title}" name="productGroup"/></th>

            </form>
        </tr>
        </c:forEach>
        </tr>
        <tr>
    </form>
    </tbody>
</table>

<table align="center">
    <tbody>
    <form name="searchProduct" method="post" action="/findProduct">
        <style>
            table, th, td {
                border: 5px solid crimson;
            }
        </style>
        <meta charset="UTF-8">
        <title></title>
        <tr>
            <th> SEARCH PRODUCT: <br/></th>
        </tr>
        <tr>
            <th><input type="text" name="oneProductSearch"/> <br/></th>
            <th><input type="submit" value="find"/></th>

        <tr>
        <tr>
    </form>
    </tbody>
</table>


<c:if test="${requestScope.findProductOnHomePage == true}">
    <table align="center">
        <tbody>
        <form name="basket" method="post" action="/basket">
            <tr>
                <th>Title</th>
                <th>Description</th>
                <th>Price</th>
                <th>Count</th>
            </tr>
            <tr>
                <c:forEach items="${requestScope.productByProductGroupOnHomePage}" var="productF">
            <tr>
                <td><c:out value="${productF.title}"></c:out></td>
                <td><c:out value="${productF.description}"></c:out></td>
                <td><c:out value="${productF.price}"></c:out></td>
                <td><c:out value="${productF.count}"></c:out></td>
                <th><input type="checkbox" value="${productF.title}" name="title"/></th>
                <th><input type="submit" value="To basket" name="title"/></th>
            </tr>
            </c:forEach>
            </tr>
        </form>
        </tbody>
    </table>
</c:if>


<c:if test="${requestScope.addToBasket == true}">
    <tbody>
    <table align="center">
        <tbody>
        <form name="findProduct" method="post" action="/homePage">
            <tr>
                <th> You add product to Basket !!!!!!!!!!</th>
                <body img src= "D:\Program\apache-tomcat-7.0.73\webapps\manager\images\ok.png" height ="50" alt="basket">
                </body>
            </tr>
        </form>
        </tbody>
    </table>
    </tbody>
</c:if>

<c:if test="${requestScope.notFindProductOnHomePage == true}">
    <tbody>
    <table align="center">
        <tbody>
        <form name="findProduct" method="post" action="/homePage">
            <tr>
                <th> Sorry, at the moment all products of this group are over !!!!!!!!!!</th>
            </tr>
        </form>
        </tbody>
    </table>
    </tbody>
</c:if>

<c:if test="${requestScope.findProduct == true}">
    <table align="center">
        <tbody>
        <form name="basket" method="post" action="/basket">
            <tr>
                <th>Title</th>
                <th>Description</th>
                <th>Price</th>
                <th>Count</th>
            </tr>
            <tr>
                <td><c:out value="${productFind.title}"></c:out></td>
                <td><c:out value="${productFind.description}"></c:out></td>
                <td><c:out value="${productFind.price}"></c:out></td>
                <td><c:out value="${productFind.count}"></c:out></td>
                <th><input type="checkbox" value="${productFind.title}" name="title"/></th>
                <th><input type="submit" value="To basket" name="title"/></th>
            </tr>
        </form>
        </tbody>
    </table>
</c:if>

<body  background= "D:\Program\apache-tomcat-7.0.73\webapps\manager\images\common.jpg" alt="imllage">
</body>

<c:if test="${requestScope.notFindProduct == true}">
    <tbody>
    <table align="center">
        <tbody>
        <form name="findProduct" method="post" action="/homePage">
            <tr>
                <th>Sorry, at the moment there is no such product !!!!!!!!!!</th>
            </tr>
        </form>
        </tbody>
    </table>
    </tbody>
</c:if>


</body>
</html>
