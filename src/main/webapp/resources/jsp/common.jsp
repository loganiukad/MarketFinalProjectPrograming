<!DOCTYPE html>
<html lang="en">
<head>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
    <title>HomePage</title>
    <style>
        table, th, td {
            border: 5px solid darkgrey;
        }
    </style>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>

<table align="right">
    <tbody>
    <form name="loginPage" method="post" action="/">
        <tr>
            <th> Username: <input type="text" name="userName"/> <br/></th>
        </tr>
        <tr>
            <th>Password: <input type="password" name="password"/> <br/></th>
        <tr>
            <th><input type="submit" value="Login to your account"/></th>
            </th>
        </tr>
        </tr>
        <tr>
    </form>
    </tbody>
</table>




<%--<html>--%>
<head>
    <meta charset="utf-8">
    <title>��� �������</title>
</head>

<body  background= "D:\Program\apache-tomcat-7.0.73\webapps\manager\images\common.jpg" alt="imllage">
<%--<p>< background= "D:\Program\apache-tomcat-7.0.73\webapps\manager\images\Ukraine.png" alt="image"></p>--%>
</body>
<%--</html>--%>



<table align="right">
    <tbody>
    <form name="registration" action="/registration">
        <tr>
            <th><input type="submit" value="Registration"/></th>
            </th>
        </tr>
        </tr>
        <tr>
    </form>
    </tbody>
</table>


<table align="left">
    <tbody>
    <form name="common" method="post" action="/findProductByProductGroups">
        <style>
            table, th, td {
                border: 5px solid crimson;
            }
        </style>
        <meta charset="UTF-8">
        <title></title>
        <tr>
            <th> Catalog: <br/></th>
        </tr>
        <tr>
            <c:forEach items="${requestScope.productGroups}" var="groups">
        <tr>
            <th><input class="" type="submit" value="${groups.title}" name="productGroup" /></th>
        </tr>
        </c:forEach>

        </tr>
        <tr>
    </form>
    </tbody>
</table>

<table align="center">
    <tbody>
    <form name="findProduct" method="post" action="/findProductCommonPage">
        <style>
            table, th, td {
                border: 5px solid crimson;
            }
        </style>
        <meta charset="UTF-8">
        <title></title>
        <tr>
            <th> SEARCH PRODUCT: <br/></th>
        </tr>
        <tr>
            <th><input type="text" name="search"/> <br/></th>
            <th><input type="submit" value="find"/></th>
        <tr>
        <tr>
    </form>
    </tbody>
</table>

<c:if test="${requestScope.notAuth == true}">
    <tbody>
    <table align="center">
        <tbody>
        <form name="findProduct" method="post" action="/homePage">
            <tr>
                <th> Sorry, you are not authorized, go to <a href="<c:url value="/registration"/> ">registration</a>
                </th>

            </tr>
        </form>
        </tbody>
    </table>
    </tbody>
</c:if>

<c:if test="${requestScope.registration == true}">
    <tbody>
    <table align="center">
        <tbody>
        <form name="myform" method="POST" action="/registration">
            <tr>
            <tr>
                <th>First name * <input type="text" name="firstName"> <br></th>
            </tr>
            <tr>
                <th>Second name * <input type="text" name="secondName"><br></th>
            </tr>
            <tr>
                <th>Birthday * <input type="date" name="birthday"><br></th>
            </tr>
            <tr>
                <th>Login * <input type="text" name="login"><br></th>
            </tr>
            <tr>
                <th>Password * <input type="password" name="password"><br></th>
            </tr>
            <tr>
                <th>Email * <input type="text" name="email"><br></th>
            </tr>
            <tr>
                <th>Sex * <input type="radio" name="sex" value="MALE">Male</input>
                    <input type="radio" name="sex" value="FEMALE">Female</input><br></th>
            </tr>
            <tr>
                <th><input type="submit" value="Save">
            </tr>
        </form>
        </tr></th>
        </form>
        </tbody>
    </table>
    </tbody>
</c:if>


<c:if test="${requestScope.findProductCommonPage == true}">
    <table align="center">
        <tbody>
        <tr>
            <th>Title</th>
            <th>Description</th>
            <th>Price</th>
            <th>Count</th>
        </tr>
        <tr>
            <td><c:out value="${requestScope.productFindCommonPage.title}"></c:out></td>
            <td><c:out value="${requestScope.productFindCommonPage.description}"></c:out></td>
            <td><c:out value="${requestScope.productFindCommonPage.price}"></c:out></td>
            <td><c:out value="${requestScope.productFindCommonPage.count}"></c:out></td>
        </tr>
        </tbody>
    </table>
</c:if>


<c:if test="${requestScope.notFindProductCommonPage == true}">
    <tbody>
    <table align="center">
        <tbody>
        <form name="findProduct" method="post" action="/homePage">
            <tr>
                <th> Sorry, at the moment this products are over !!!!!!!!!!</th>
            </tr>
        </form>
        </tbody>
    </table>
    </tbody>
</c:if>

<c:if test="${requestScope.findProduct == true}">
    <table align="center">
        <tbody>
        <tr>
            <th>Title</th>
            <th>Description</th>
            <th>Price</th>
            <th>Count</th>
        </tr>
        <tr>
            <c:forEach items="${requestScope.productFind}" var="productF">
        <tr>
            <td><c:out value="${productF.title}"></c:out></td>
            <td><c:out value="${productF.description}"></c:out></td>
            <td><c:out value="${productF.price}"></c:out></td>
            <td><c:out value="${productF.count}"></c:out></td>
        </tr>
        </c:forEach>
        </tr>
        </tbody>
    </table>
</c:if>

<c:if test="${requestScope.notFindProductByProductGroup == true}">
    <tbody>
    <table align="center">
        <tbody>
        <form name="findProduct" method="post" action="/homePage">
            <tr>
                <th> Sorry, at the moment all products of this group are over !!!!!!!!!!</th>
            </tr>
        </form>
        </tbody>
    </table>
    </tbody>
</c:if>

<c:if test="${requestScope.findProductByProductGroup == true}">
    <table align="center">
        <tbody>
        <tr>
            <th>Title</th>
            <th>Description</th>
            <th>Price</th>
            <th>Count</th>
        </tr>
        <tr>
            <c:forEach items="${requestScope.productByProductGroup}" var="productF">
        <tr>
            <td><c:out value="${productF.title}"></c:out></td>
            <td><c:out value="${productF.description}"></c:out></td>
            <td><c:out value="${productF.price}"></c:out></td>
            <td><c:out value="${productF.count}"></c:out></td>
        </tr>
        </c:forEach>
        </tr>
        </tbody>
    </table>
</c:if>

</body>
</html>


</body>
</html>


