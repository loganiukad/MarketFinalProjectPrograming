<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
<head>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
    <title>Home Page</title>
    <style>
        table, th, td {
            border: 4px solid darkorange;
        }
    </style>
</head>
<body>

<c:if test="${sessionScope.addProductGroup == true}">
    Such product exists !!!
</c:if>

<table align="left">
    <tbody>
    <form name="findByProductGroupSetting" method="post" action="/findByProductGroupSetting">
        <style>
            table, th, td {
                border: 5px solid crimson;
            }
        </style>
        <meta charset="UTF-8">
        <title></title>
        <tr>
            <th> Product Groups: <br/></th>
        </tr>
        <tr>
            <c:forEach items="${sessionScope.productGroups}" var="groups">
        <tr>
            <form name="findByProductGroupSetting" method="post" action="/findByProductGroupSetting">
                <th><input type="submit" value="${groups.title}" name="findByProductGroupSetting"/></th>
            </form>
        </tr>
        </c:forEach>
        </tr>
        <tr>
    </form>
    </tbody>
</table>

<table align="left">
    <tbody>
    <form name="createProduct" method="post" action="/productSetting">
        <style>
            table, th, td {
                border: 5px solid crimson;
            }
        </style>
        <meta charset="UTF-8">
        <tr>
            <th> Add new product: <br/></th>
        </tr>
        <tr>
        <tr>
            <td>Title * <input type="text" name="title"> <br></td>
        </tr>
        <tr>
            <td>Description * <input type="text" name="description"><br></td>
        </tr>
        <tr>
            <td>Price * <input type="text" name="price"><br></td>
        </tr>
        <tr>
            <td>Count * <input type="text" name="count"><br></td>
        </tr>
        <tr>
            <td>Group * <input type="text" name="productGroup"><br></td>
        </tr>
        <tr>
            <td><input type="submit" value="Save product ">
    </form>
    </td></tr>
    </tr>

    </form>
    </tbody>
</table>

<table align="left">
    <tbody>
    <form name="findProductByAdmin" method="post" action="/findByProductSetting">
        <th><input type="submit" value="Find Product"/></th>
        <th><input type="text" name="title"/> <br/></th>
    </form>
    </tbody>
</table>

<c:if test="${requestScope.findProduct == true}">
    <table align="center">
        <tbody>
        <tr>
            <th>Title</th>
            <th>Description</th>
            <th>Price</th>
            <th>Count</th>
            <th>Group</th>
        </tr>
        <tr>
            <td><c:out value="${productFindOnSetting.title}"></c:out></td>
            <td><c:out value="${productFindOnSetting.description}"></c:out></td>
            <td><c:out value="${productFindOnSetting.price}"></c:out></td>
            <td><c:out value="${productFindOnSetting.count}"></c:out></td>

            <td><c:out value="${productFindOnSetting.productGroup}"></c:out></td>
            <td><c:forEach items="${productFindOnSetting.productGroup}" var="groups">
                ${groups[0].title}
            </c:forEach></td>
        <tr>
        </tr>
        </tbody>
    </table>
</c:if>

<c:if test="${requestScope.notFindProduct == true}">
    <tbody>
    <table align="center">
        <tbody>
        <form name="findProduct" action="/productSetting">
            <tr>
                <th> Sorry, at the moment this products are over !!!!!!!!!!</th>
            </tr>
        </form>
        </tbody>
    </table>
    </tbody>
</c:if>

<c:if test="${requestScope.updateProduct == true}">
    <tbody>
    <table align="center">
        <tbody>
        <form name="findProduct" action="/productSetting">
            <tr>
                <th> You update product !!!!!!!!!!</th>
            </tr>
        </form>
        </tbody>
    </table>
    </tbody>
</c:if>

<c:if test="${requestScope.updateProduct == true}">
    <table align="center">
        <tbody>
        <tr>
            <th>Title</th>
            <th>Description</th>
            <th>Price</th>
            <th>Count</th>
            <th>Group</th>
        </tr>
        <tr>
            <c:forEach items="${requestScope.update}" var="productUpdate">
        <tr>
            <td><c:out value="${productUpdate.title}"></c:out></td>
            <td><c:out value="${productUpdate.description}"></c:out></td>
            <td><c:out value="${productUpdate.price}"></c:out></td>
            <td><c:out value="${productUpdate.count}"></c:out></td>
            <td><c:out value="${productUpdate.productGroup}"></c:out></td>
        </tr>
        </c:forEach>
        </tbody>
    </table>
</c:if>

<c:if test="${requestScope.notFindProductByProductGroupSetting == true}">
    <tbody>
    <table align="center">
        <tbody>
        <form name="findProduct" action="/productSetting">
            <tr>
                <th> Sorry, at the moment all products of this group are over !!!!!!!!!!</th>
            </tr>
        </form>
        </tbody>
    </table>
    </tbody>
</c:if>

<c:if test="${requestScope.findProductByProductGroupSetting == true}">
    <table align="center">
        <tbody>
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Description</th>
            <th>Price</th>
            <th>Count</th>
            <th>Group</th>
        </tr>
        <tr>
            <c:forEach items="${requestScope.productByProductGroupSetting}" var="productsSetting">
        <tr>
            <form name="updateProductByAdmin" method="post" action="/updateProductByAdmin">
                <th><input type="text" value="${productsSetting.id}" name="id"/></th>
                <th><input type="text" value="${productsSetting.title}" name="title"/></th>
                <th><input type="text" value="${productsSetting.description}" name="description"/></th>
                <th><input type="text" value="${productsSetting.price}" name="price"/></th>
                <th><input type="int" value="${productsSetting.count}" name="count"/></th>
                <th><input type="text" value="${productsSetting.productGroup}" name="group"/></th>
                <th><input type="submit" value="update"/></th>
            </form>
            <form name="deleteProduct" method="post" action="/deleteProduct">
                <th><input type="text" value="${productsSetting.id}" name="id"/></th>
                <th><input type="text" value="${productsSetting.title}" name="title"/></th>
                <th><input type="text" value="${productsSetting.description}" name="description"/></th>
                <th><input type="text" value="${productsSetting.price}" name="price"/></th>
                <th><input type="int" value="${productsSetting.count}" name="count"/></th>
                <th><input type="text" value="${productsSetting.productGroup}" name="group"/></th>
                <th><input type="submit" value="delete"/></th>
            </form>
            </th>
        </tr>
        </c:forEach>
        </tr>
        </tbody>
    </table>
</c:if>

<c:if test="${requestScope.addNewProduct == true}">
    <tbody>
    <table align="center">
        <tbody>
        <form name="findProduct" action="/productSetting">
            <tr>
                <th>You add new product !!!!</th>
            </tr>
        </form>
        </tbody>
    </table>
    </tbody>
</c:if>

<c:if test="${requestScope.deleteProduct == true}">
    You delete product !!!!
</c:if>

<c:if test="${requestScope.update == false}">
    Update finish !!!
</c:if>

</body>
</html>

