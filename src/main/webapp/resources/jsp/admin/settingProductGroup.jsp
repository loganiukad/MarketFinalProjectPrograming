
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
<head>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
    <title>Home Page</title>
    <style>
        table, th, td {
            border: 4px solid darkorange;
        }
    </style>
</head>
<body>

<h4>Add new product group</h4>

<form name="myform" method="POST" action="/productGroupSetting">

    Title * <input type="text" name="title"> <br>
    Description * <input type="text" name="description"><br>
    <input type="submit" value="Save product group"></form>
</form>

<c:if test="${requestScope.allProductGroupsShow == false}">
    <table align="center">
        <tbody>
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Description</th>
        </tr>
        <tr>
            <c:forEach items="${requestScope.allProductGroups}" var="groups">
        <tr>
                <form name="updateProductGroupByAdmin" method="post" action="/updateProductGroupByAdmin">
            <th><input type="text" value="${groups.id}" name="id"/></th>
            <th><input type="text" value="${groups.title}" name="title"/></th>
            <th><input type="text" value="${groups.description}" name="description"/></th>
            <th><input type="submit" value="update"/></th>
            </form>
            <form name="deleteProductGroup" method="post" action="/deleteProductGroup">
                <th><input type="text" value="${groups.id}" name="id"/></th>
                <th><input type="text" value="${groups.title}" name="title"/></th>
                <th><input type="text" value="${groups.description}" name="description"/></th>
                <th><input type="submit" value="delete"/></th>
                 </form>
            </th>
        </tr>
        </c:forEach>
        </tr>
        </tbody>
    </table>
</c:if>

<c:if test="${requestScope.addProductGroup == true}">
    <table align="center">
        <tbody>
        <h4> You add new productGroup</h4>
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Description</th>
        </tr>
        <tr>
            <c:forEach items="${requestScope.productGroupAdd}" var="groupsAdd">
        <tr>
            <form name="updateProductGroupByAdmin" method="post" action="/updateProductGroupByAdmin">
                <th><input type="text" value="${groupsAdd.id}" name="id"/></th>
                <th><input type="text" value="${groupsAdd.title}" name="title"/></th>
                <th><input type="text" value="${groupsAdd.description}" name="description"/></th>
                <th><input type="submit" value="update"/></th>
            </form>
            <form name="deleteProductGroup" method="post" action="/deleteProductGroup">
                <th><input type="text" value="${groupsAdd.id}" name="id"/></th>
                <th><input type="text" value="${groupsAdd.title}" name="title"/></th>
                <th><input type="text" value="${groupsAdd.description}" name="description"/></th>
                <th><input type="submit" value="delete"/></th>
            </form>
            </th>
        </tr>
        </c:forEach>
        </tr>
        </tbody>
    </table>
</c:if>

<c:if test="${requestScope.updateProductGroup == true}">
    <table align="center">
        <tbody>
        <h4> You update productGroup with ID = <c:out value="${requestScope.update.id}"></c:out></h4>
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Description</th>
        </tr>
        <tr>
            <c:forEach items="${requestScope.productGroupUpdate}" var="groupUpdate">
        <tr>
            <form name="updateProductGroupByAdmin" method="post" action="/updateProductGroupByAdmin">
                <th><input type="text" value="${groupUpdate.id}" name="id"/></th>
                <th><input type="text" value="${groupUpdate.title}" name="title"/></th>
                <th><input type="text" value="${groupUpdate.description}" name="description"/></th>
                <th><input type="submit" value="update"/></th>
            </form>
            <form name="deleteProductGroup" method="post" action="/deleteProductGroup">
                <th><input type="text" value="${groupUpdate.id}" name="id"/></th>
                <th><input type="text" value="${groupUpdate.title}" name="title"/></th>
                <th><input type="text" value="${groupUpdate.description}" name="description"/></th>
                <th><input type="submit" value="delete"/></th>
            </form>
            </th>
        </tr>
        </c:forEach>
        </tr>
        </tbody>
    </table>
</c:if>


<c:if test="${requestScope.deleteProductGroup == true}">
    <table align="center">
        <tbody>
        <h4> You delete productGroup </h4>
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Description</th>
        </tr>
        <tr>
            <c:forEach items="${requestScope.productGroupDelete}" var="groupDelete">
        <tr>
            <form name="updateProductGroupByAdmin" method="post" action="/updateProductGroupByAdmin">
                <th><input type="text" value="${groupDelete.id}" name="id"/></th>
                <th><input type="text" value="${groupDelete.title}" name="title"/></th>
                <th><input type="text" value="${groupDelete.description}" name="description"/></th>
                    <th><input type="submit" value="update"/></th>
            </form>
            <form name="deleteProductGroup" method="post" action="/deleteProductGroup">
                <th><input type="text" value="${groupDelete.id}" name="id"/></th>
                <th><input type="text" value="${groupDelete.title}" name="title"/></th>
                <th><input type="text" value="${groupDelete.description}" name="description"/></th>
                    <th><input type="submit" value="delete"/></th>
            </form>
            </th>
        </tr>
        </c:forEach>
        </tr>
        </tbody>
    </table>

</c:if>

<c:if test="${requestScope.notUpdateProductGroup == true}">
    Such a group exists !!!!!
</c:if>

<c:if test="${requestScope.notAddProductGroup == true}">
    This product group has been created !!!!!!!!!
</c:if>

<c:if test="${requestScope.updateProductGroupByA == true}">
    Update finish !!!
</c:if>

</body>
</html>
