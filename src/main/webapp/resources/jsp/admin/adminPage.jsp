<!DOCTYPE html>
<html lang="en">
<head>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
    <title>admin page</title>
    <style>
        table, th, td {
            border: 5px solid darkgrey;
        }
    </style>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>

<table align="left">
    <tbody>
    <form name="findProduct" method="post" action="/findProductByProductGroupByAdmin">
        <style>
            table, th, td {
                border: 5px solid crimson;
            }
        </style>
        <meta charset="UTF-8">
        <title></title>
        <tr>
            <th> Product Groups: <br/></th>
        </tr>
        <tr>
            <c:forEach items="${sessionScope.productGroups}" var="groups">
        <tr>
            <form name="findProductByGroups" method="post" action="/findProductByProductGroupByAdmin">
                <th><input type="submit" value="${groups.title}" name = "findProductByGroupByAdmin"/></th>

            </form>
        </tr>
        </c:forEach>
        </tr>
        <tr>
    </form>
    </tbody>
</table>

<c:if test="${requestScope.findProduct == true}">
    <table align="center">
        <tbody>
        <tr>
            <th>Title</th>
            <th>Description</th>
            <th>Price</th>
            <th>Count</th>
        </tr>
        <tr>
            <td><c:out value="${requestScope.product.title}"></c:out></td>
            <td><c:out value="${requestScope.product.description}"></c:out></td>
            <td><c:out value="${requestScope.product.price}"></c:out></td>
            <td><c:out value="${requestScope.product.count}"></c:out></td>
        </tr>
        </tbody>
    </table>
</c:if>

<c:if test="${requestScope.findProductByProductGroupByAdmin == true}">
    <table align="center">
        <tbody>
        <tr>
            <th>Title</th>
            <th>Description</th>
            <th>Price</th>
            <th>Count</th>
        </tr>
        <tr>
            <c:forEach items="${requestScope.productByProductGroupByAdmin}" var="productAdminPage">
        <tr>
            <td><c:out value="${productAdminPage.title}"></c:out></td>
            <td><c:out value="${productAdminPage.description}"></c:out></td>
            <td><c:out value="${productAdminPage.price}"></c:out></td>
            <td><c:out value="${productAdminPage.count}"></c:out></td>
        </tr>
        </c:forEach>
        </tr>
        </tbody>
    </table>
</c:if>

<c:if test="${requestScope.notFindProductByProductGroupByAdmin == true}">
    <tbody>
    <table align="center">
        <tbody>
        <form name="findProduct" method="post" action="/homePage">
            <tr>
                <th> Sorry, at the moment all products of this group are over !!!!!!!!!!</th>
            </tr>
        </form>
        </tbody>
    </table>
    </tbody>
</c:if>

<form name="addProductByAdmin" action="/productSetting">
    <input type="submit" value="Product setting"/>

</form>

<form name="addProductGroupByAdmin" action="/productGroupSetting">

    <input type="submit" value="Product Group setting"/>
</form>

</body>
</html>
