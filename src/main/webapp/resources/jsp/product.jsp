<%@ page language="java" contentType="text/html; charset=US-ASCII"
         pageEncoding="US-ASCII" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
    <title>Home Page</title>
    <style>
        table, th, td {
            border: 10px solid darkorange;
        }
    </style>
</head>
<body>
<table align="center">
    <tbody>
    <tr>
        <th>Title</th>
        <th>Description</th>
        <th>Price</th>
        <th>Count</th>
    </tr>
    <tr>
        <td><c:out value="${sessionScope.product.title}"></c:out></td>
        <td><c:out value="${sessionScope.product.description}"></c:out></td>
        <td><c:out value="${sessionScope.product.price}"></c:out></td>
        <td><c:out value="${sessionScope.product.count}"></c:out></td>
    </tr>
    </tbody>
</table>

</body>
</html>