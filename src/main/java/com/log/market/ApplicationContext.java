package com.log.market;

import com.log.market.datasource.DataSource;

import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 * Created by User on 10.11.2016.
 */
public class ApplicationContext {
    private static DataSource dataSource;
    private static String tableUsers = "CREATE TABLE IF NOT EXISTS Users (id INT(100) NOT NULL AUTO_INCREMENT, firstName VARCHAR (50),secondName VARCHAR (50), birthday DATE, login VARCHAR(50),password VARCHAR (50),email VARCHAR (50),sex INT (2),role INT (2),PRIMARY KEY(id))";
    private static String tableProducts = "CREATE TABLE IF NOT EXISTS Products (id INT(100) NOT NULL AUTO_INCREMENT, title VARCHAR (200),description VARCHAR (200),price VARCHAR (50),count INT ,productGroup VARCHAR (50), PRIMARY KEY(id))";
    private static String tableProductGroups = "CREATE TABLE IF NOT EXISTS ProductGroups (id INT(100) NOT NULL AUTO_INCREMENT, title VARCHAR (200),description VARCHAR (200),product VARCHAR (50),PRIMARY KEY(id))";
    private static String tableTransaction = "CREATE TABLE IF NOT EXISTS Transactions (id INT(100) NOT NULL AUTO_INCREMENT, product VARCHAR (200),productCount INT (200),productPrice DOUBLE,date DATE, PRIMARY KEY(id))";
    private static String tableProductGroupToProduct = "CREATE TABLE IF NOT EXISTS ProductGroupToProduct(id INT(100) NOT NULL AUTO_INCREMENT,productID INT (100),product VARCHAR(50),productGroupID INT (100), productGroup VARCHAR(50), PRIMARY KEY (id))";

    static void init() {
    }


    public static void createTableProducts() throws Exception {
        {
            try {
                Connection connection = dataSource.createConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(tableProducts);
                preparedStatement.executeUpdate();
            } catch (Exception e) {
                System.out.println(e);
            } finally {
                System.out.println("Create tebleProduct complit");
            }
        }
    }

    public static void createTableProductGroups() throws Exception {
        {
            try {
                Connection connection = dataSource.createConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(tableProductGroups);
                preparedStatement.executeUpdate();
            } catch (Exception e) {
                System.out.println(e);
            } finally {
                System.out.println("Create tebleProductGroups complit");
            }
        }
    }

    public static void createTableUsers() throws Exception {
        {
            try {
                Connection connection = dataSource.createConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(tableUsers);
                preparedStatement.executeUpdate();
            } catch (Exception e) {
                System.out.println(e);
            } finally {
                System.out.println("Create tebleUsers complit");
            }
        }
    }

    public static void createTableTransactions() throws Exception {
        {
            try {
                Connection connection = dataSource.createConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(tableTransaction);
                preparedStatement.executeUpdate();
            } catch (Exception e) {
                System.out.println(e);
            } finally {
                System.out.println("Create tebleTransaction complit");
            }
        }
    }

    public static void createTableProductGroupToProduct() throws Exception {
        {
            try {
                Connection connection = dataSource.createConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(tableProductGroupToProduct);
                preparedStatement.executeUpdate();
            } catch (Exception e) {
                System.out.println(e);
            } finally {
                System.out.println("Create tebleManyToMany complit");
            }
        }
    }

}