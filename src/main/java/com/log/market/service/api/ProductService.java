package com.log.market.service.api;

import com.log.market.dto.ProductDTO;
import com.log.market.dto.ProductGroupDTO;

import java.util.List;

/**
 * Created by User on 27.10.2016.
 */
public interface ProductService {

    ProductDTO findProductById(Integer id);

    ProductDTO createProduct(ProductDTO product);

    ProductDTO updateProduct(ProductDTO product);

    boolean deleteProduct(Integer id);

    List<ProductGroupDTO> findProductGroups(Integer id);

    ProductDTO findByTitle (String title);

    ProductDTO addToBasket(String title);

    ProductDTO deleteFromBasket(String title);

    boolean returnCount(ProductDTO product);

}
