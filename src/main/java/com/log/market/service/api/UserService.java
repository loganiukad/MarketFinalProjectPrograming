package com.log.market.service.api;

import com.log.market.dto.UserDTO;

/**
 * Created by User on 10.10.2016.
 */
public interface UserService {


    UserDTO findByLoginAndPassword(String login, String password);

    UserDTO findByLoginAndEmale(String login, String email);

    UserDTO createUser(UserDTO user);

    boolean deleteUser(Integer id);

    UserDTO updateUser(UserDTO user);


}
