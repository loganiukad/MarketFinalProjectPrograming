package com.log.market.service.api;

import com.log.market.dto.ProductDTO;
import com.log.market.dto.ProductGroupDTO;

import java.util.List;

/**
 * Created by User on 27.10.2016.
 */
public interface ProductGroupService {

    ProductGroupDTO findProductGroupById(Integer id);

    ProductGroupDTO createProductGroup(ProductGroupDTO productGroup);

    ProductGroupDTO updateProductGroup(ProductGroupDTO productGroup);

    boolean deleteProductGroup(Integer id);

    List<ProductDTO> findProducts(Integer id);

    List<ProductGroupDTO> findAllProductGroups();

    ProductGroupDTO findProductGroupByTitle(String title);

    List<ProductGroupDTO> findProductGroupByListTitle(List<String> title);
}
