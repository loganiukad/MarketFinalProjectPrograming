package com.log.market.service.api;

import com.log.market.dto.TransactionDTO;

/**
 * Created by User on 27.10.2016.
 */
public interface TransactionService {

    TransactionDTO findTransactionById(Integer id);

    TransactionDTO createTransaction(TransactionDTO transaction);

    TransactionDTO updateTransaction(TransactionDTO transaction);

    boolean deleteTransaction(Integer id);

}
