package com.log.market.service.impl;

import com.log.market.dao.api.ProductGroupDAO;
import com.log.market.dao.impl.ProductGroupDAOImpl;
import com.log.market.dto.ProductDTO;
import com.log.market.dto.ProductGroupDTO;
import com.log.market.entity.Product;
import com.log.market.entity.ProductGroup;
import com.log.market.helper.Transformer;
import com.log.market.service.api.ProductGroupService;

import java.util.List;

/**
 * Created by User on 27.10.2016.
 */
public class ProductGroupServiceImpl implements ProductGroupService {


    private static volatile ProductGroupService instance;
    private static ProductGroupDAO productGroupDAO;

    private ProductGroupServiceImpl() {
    }

    public static ProductGroupService getInstance() {
        if (instance == null) {
            synchronized (ProductGroupServiceImpl.class) {
                if (instance == null) {
                    instance = new ProductGroupServiceImpl();
                    productGroupDAO = ProductGroupDAOImpl.getInstance();
                }
            }
        }
        return instance;
    }


    public ProductGroupDTO findProductGroupById(Integer id) {
        ProductGroup productGroupById = productGroupDAO.findProductGroupById(id);
        ProductGroupDTO productGroupDTO = Transformer.transformProductGroupToProductGroupDTO(productGroupById);
        return productGroupDTO;
    }

    public ProductGroupDTO createProductGroup(ProductGroupDTO productGroup) {
        ProductGroupDTO resProductGroupDTO = null;
        ProductGroup productGroupByTitle = productGroupDAO.findProductGroupByTitle(productGroup.getTitle());
        if (productGroupByTitle == null) {
            ProductGroup createProductGroup = productGroupDAO.create(Transformer.transformProductGroupDTOToProductGroup(productGroup));
            if (createProductGroup != null) {
                resProductGroupDTO = Transformer.transformProductGroupToProductGroupDTO(createProductGroup);
            }
        }
        return resProductGroupDTO;
    }

    public ProductGroupDTO updateProductGroup(ProductGroupDTO productGroup) {
        ProductGroupDTO productGroupDTO = null;
        ProductGroup updateProductGroup = productGroupDAO.update(Transformer.transformProductGroupDTOToProductGroup(productGroup));
        if (updateProductGroup != null) {
            productGroupDTO = Transformer.transformProductGroupToProductGroupDTO(updateProductGroup);
        }
        return productGroupDTO;
    }

    public boolean deleteProductGroup(Integer id) {
        boolean productGroup = productGroupDAO.delete(id);
        return productGroup;
    }

    public List<ProductDTO> findProducts(Integer id) {
        List<ProductDTO> productDTO = null;
        List<Product> findProducts = productGroupDAO.findProducts(id);
        if (findProducts.size() != 0) {
            productDTO = Transformer.transformListProductToListProductDTO(findProducts);
        }
        return productDTO;
    }

    @Override
    public List<ProductGroupDTO> findAllProductGroups() {
        List<ProductGroupDTO> productGroupDTO = null;
        List<ProductGroup> allProductGroups = productGroupDAO.findAllProductGroups();
        if (allProductGroups != null) {
            productGroupDTO = Transformer.transformListProductGroupToListProductGroupDTO(allProductGroups);
        }
        return productGroupDTO;
    }

    @Override
    public ProductGroupDTO findProductGroupByTitle(String title) {
        ProductGroupDTO productGroupDTO = null;
        ProductGroup productGroup = productGroupDAO.findProductGroupByTitle(title);
        if (productGroup != null) {
            productGroupDTO = Transformer.transformProductGroupToProductGroupDTO(productGroup);
        }
        return productGroupDTO;
    }

    @Override
    public List<ProductGroupDTO> findProductGroupByListTitle(List<String> title) {
        List<ProductGroupDTO> productGroupDTOs = null;
        List<ProductGroup> productGroupList = productGroupDAO.findProductGroupByListTitle(title);
        if (productGroupList != null) {
            productGroupDTOs = Transformer.transformListProductGroupToListProductGroupDTO(productGroupList);
        }
        return productGroupDTOs;
    }
}
