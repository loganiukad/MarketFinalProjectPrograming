package com.log.market.service.impl;

import com.log.market.dao.api.ProductDAO;
import com.log.market.dao.impl.ProductDAOImpl;
import com.log.market.dto.ProductDTO;
import com.log.market.dto.ProductGroupDTO;
import com.log.market.entity.Product;
import com.log.market.entity.ProductGroup;
import com.log.market.helper.Transformer;
import com.log.market.service.api.ProductService;

import java.util.List;

/**
 * Created by User on 27.10.2016.
 */
public class ProductServiceImpl implements ProductService {

    private static volatile ProductService instance;
    private static ProductDAO productDAO;

    private ProductServiceImpl() {
    }

    public static ProductService getInstance() {
        if (instance == null) {
            synchronized (ProductServiceImpl.class) {
                if (instance == null) {
                    instance = new ProductServiceImpl();
                    productDAO = ProductDAOImpl.getInstance();
                }
            }
        }
        return instance;
    }

    public ProductDTO findProductById(Integer id) {
        Product productById = productDAO.findProductById(id);
        ProductDTO productDTO = Transformer.transformProductToProductDTO(productById);
        return productDTO;
    }

    public ProductDTO createProduct(ProductDTO productDTO) {
        ProductDTO resProductDTO = null;
        if (!productDAO.isExistProduct(productDTO.getTitle())) {
            Product createProduct = productDAO.create(Transformer.transformProductDTOToProduct(productDTO));
            if (createProduct != null) {
                resProductDTO = Transformer.transformProductToProductDTO(createProduct);
            }
        }
        return resProductDTO;
    }

    public ProductDTO updateProduct(ProductDTO product) {
        Product updateProduct = productDAO.update(Transformer.transformProductDTOToProduct(product));
        ProductDTO productDTO = Transformer.transformProductToProductDTO(updateProduct);
        return productDTO;
    }

    public boolean deleteProduct(Integer id) {
        boolean product = productDAO.delete(id);
        return product;
    }

    public List<ProductGroupDTO> findProductGroups(Integer id) {
        List<ProductGroup> findProductGroups = productDAO.findProductGroups(id);
        List<ProductGroupDTO> productGroupDTOs = Transformer.transformListProductGroupToListProductGroupDTO(findProductGroups);
        return productGroupDTOs;
    }

    @Override
    public ProductDTO findByTitle(String title) {
        Product productByTitle = productDAO.findProductByTitle(title);
        ProductDTO productDTO = null;
        if (productByTitle != null) {
            productDTO = Transformer.transformProductToProductDTO(productByTitle);
        }

        return productDTO;
    }


    public ProductDTO addToBasket(String title) {
        Product add = productDAO.addToBasket(title);
        ProductDTO productDTO = Transformer.transformProductToProductDTO(add);
        return productDTO;
    }

    @Override
    public ProductDTO deleteFromBasket(String title) {
        Product product = productDAO.deleteFromBasket(title);
        ProductDTO productDTO = Transformer.transformProductToProductDTO(product);
        return productDTO;
    }

    @Override
    public boolean returnCount(ProductDTO product) {
        boolean product1 = productDAO.returnCount(Transformer.transformProductDTOToProduct(product));
        return product1;

    }
}
