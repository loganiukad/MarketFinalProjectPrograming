package com.log.market.service.impl;

import com.log.market.dao.api.TransactionDAO;
import com.log.market.dao.impl.TransactionDAOImpl;
import com.log.market.dto.TransactionDTO;
import com.log.market.entity.Transaction;
import com.log.market.helper.Transformer;
import com.log.market.service.api.TransactionService;

/**
 * Created by User on 27.10.2016.
 */
public class TransactionServiceImpl implements TransactionService {

    private static volatile TransactionService instance;
    private static TransactionDAO transactionDAO;
    private TransactionServiceImpl() {
    }

    public static TransactionService getInstance(){
        if (instance == null){
            synchronized (TransactionServiceImpl.class){
                if(instance == null){
                    instance = new TransactionServiceImpl();
                    transactionDAO = TransactionDAOImpl.getInstance();

                }
            }
        }
        return instance;
    }

    public TransactionDTO findTransactionById(Integer id) {
        Transaction transactionById = transactionDAO.findTransactionById(id);
        TransactionDTO transactionDTO = Transformer.transformTransactionToTransactionDTO(transactionById);


        return transactionDTO;
    }

    public TransactionDTO createTransaction(TransactionDTO transaction) {
        Transaction createTransaction = transactionDAO.create(Transformer.transformTransactionDTOToTransaction(transaction));
        if (createTransaction == null) {
            transaction = null;
        }
        return transaction;
    }

    public TransactionDTO updateTransaction(TransactionDTO transaction) {
        Transaction updateTransaction = transactionDAO.updateTransaction(Transformer.transformTransactionDTOToTransaction(transaction));
        TransactionDTO transactionDTO = Transformer.transformTransactionToTransactionDTO(updateTransaction);
        return transactionDTO;
    }

    public boolean deleteTransaction(Integer id) {
        boolean transaction = transactionDAO.deleteTransaction(id);
        return transaction;
    }
}
