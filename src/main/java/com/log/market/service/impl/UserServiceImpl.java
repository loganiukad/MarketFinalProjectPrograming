package com.log.market.service.impl;

import com.log.market.dao.api.UserDAO;
import com.log.market.dao.impl.UserDAOImpl;
import com.log.market.dto.UserDTO;
import com.log.market.entity.User;
import com.log.market.helper.Transformer;
import com.log.market.service.api.UserService;

/**
 * Created by User on 10.10.2016.
 */
public class UserServiceImpl implements UserService {

    private static volatile UserService instance;
    private static UserDAO userDAO;

    private UserServiceImpl() {
    }

    public static UserService getInstance() {

        if (instance == null) {
            synchronized (UserServiceImpl.class) {
                if (instance == null) {
                    instance = new UserServiceImpl();
                    userDAO = UserDAOImpl.getInstance();
                }
            }
        }
        return instance;
    }

    public UserDTO findByLoginAndPassword(String login, String password) {
        UserDTO userResDTO = null;
        User user = userDAO.findByLoginAndPassword(login, password);
        if (user != null) {
            userResDTO = Transformer.transformUsersToUsersDTO(user);
        }
        return userResDTO;
    }

    public UserDTO findByLoginAndEmale(String login, String emale) {
        User user = userDAO.findByLoginAndEmail(login, emale);
        UserDTO userDTO = Transformer.transformUsersToUsersDTO(user);
        return userDTO;
    }

    @Override
    public UserDTO createUser(UserDTO userDTO) {
        UserDTO resUserDTO = null;
        if (!userDAO.isLoginExist(userDTO.getLogin())) {
            User createdUser = userDAO.create(Transformer.transformUserDTOToUser(userDTO));
            if (createdUser != null) {
                resUserDTO = Transformer.transformUsersToUsersDTO(createdUser);
            }
        }
        return resUserDTO;
    }

    public boolean deleteUser(Integer id) {
        boolean user = userDAO.delete(id);
        return user;
    }

    @Override
    public UserDTO updateUser(UserDTO user) {
        UserDTO resUserDTO = null;
        if (!userDAO.isLoginExist(user.getLogin())) {
            User updateUser = userDAO.update(Transformer.transformUserDTOToUser(user));
            if (updateUser != null) {
                resUserDTO = Transformer.transformUsersToUsersDTO(updateUser);
            }
        }
        return resUserDTO;
    }
}