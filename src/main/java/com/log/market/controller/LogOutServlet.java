package com.log.market.controller;

import com.log.market.dto.ProductDTO;
import com.log.market.dto.ProductGroupDTO;
import com.log.market.service.api.ProductGroupService;
import com.log.market.service.api.ProductService;
import com.log.market.service.impl.ProductGroupServiceImpl;
import com.log.market.service.impl.ProductServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by User on 06.12.2016.
 */
public class LogOutServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        ProductService productService = ProductServiceImpl.getInstance();
        List<ProductDTO> addProductToBasket = (LinkedList) session.getAttribute("addProductToBasket");
        if (addProductToBasket != null) {
            for (ProductDTO productDTO : addProductToBasket) {
                productService.returnCount(productDTO);
            }
        }
        ProductGroupService productGroupService = ProductGroupServiceImpl.getInstance();
        List<ProductGroupDTO> productGroupDTOList = productGroupService.findAllProductGroups();
        if (productGroupDTOList != null) {
            request.setAttribute("productGroups", productGroupDTOList);
        }
        session.invalidate();
        request.getRequestDispatcher("/resources/jsp/common.jsp").forward(request, response);
    }
}
