package com.log.market.controller;

import com.log.market.dto.ProductGroupDTO;
import com.log.market.service.api.ProductGroupService;
import com.log.market.service.impl.ProductGroupServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by User on 08.12.2016.
 */
public class DeleteProductGroupByAdminServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/resources/jsp/admin/settingProductGroup.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Integer id = Integer.parseInt(request.getParameter("id"));
        String title = request.getParameter("title");
        String description = request.getParameter("description");
        ProductGroupService productGroupService = ProductGroupServiceImpl.getInstance();
        ProductGroupDTO productGroupDTO = new ProductGroupDTO();
        productGroupDTO.setId(id);
        productGroupDTO.setTitle(title);
        productGroupDTO.setDescription(description);
        boolean productGroup = productGroupService.deleteProductGroup(id);
        List<ProductGroupDTO> productGroupDTO1 = productGroupService.findAllProductGroups();
        request.setAttribute("deleteProductGroup", true);
        request.setAttribute("productGroupDelete", productGroupDTO1);
        request.getRequestDispatcher("/resources/jsp/admin/settingProductGroup.jsp").forward(request, response);
    }
}
