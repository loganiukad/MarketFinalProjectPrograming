package com.log.market.controller;

import com.log.market.dto.ProductDTO;
import com.log.market.service.api.ProductService;
import com.log.market.service.impl.ProductServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by User on 07.12.2016.
 */
public class FindProductByAdmin extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/resources/jsp/admin/adminPage.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String title = request.getParameter("title");
        ProductService productService = ProductServiceImpl.getInstance();
        ProductDTO productDTO = productService.findByTitle(title);
        HttpSession session = request.getSession();
        if (productDTO != null) {
            request.getSession().setAttribute("findProduct", false);
            session.setAttribute("product", productDTO);
            request.getRequestDispatcher("/resources/jsp/admin/adminPage.jsp").forward(request, response);
        } else {
            request.getRequestDispatcher("/resources/jsp/admin/adminPage.jsp").forward(request, response);
        }
    }
}
