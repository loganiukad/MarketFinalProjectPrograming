package com.log.market.controller;

import com.log.market.dto.ProductDTO;
import com.log.market.dto.ProductGroupDTO;
import com.log.market.service.api.ProductGroupService;
import com.log.market.service.api.ProductService;
import com.log.market.service.impl.ProductGroupServiceImpl;
import com.log.market.service.impl.ProductServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by User on 29.11.2016.
 */
public class SettingProductServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/resources/jsp/admin/settingProduct.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String title = request.getParameter("title");
        String description = request.getParameter("description");
        String price = request.getParameter("price");
        String count = request.getParameter("count");
        String productGroup = request.getParameter("productGroup");
        String[] pr = productGroup.split(",");
        List<String> productGroupList = new LinkedList<String>();
        for (String s : pr) {
            productGroupList.add(s);
        }
        ProductService productService = ProductServiceImpl.getInstance();
        ProductDTO productDTO = new ProductDTO();
        productDTO.setTitle(title);
        productDTO.setDescription(description);
        productDTO.setPrice(price);
        ProductGroupService productGroupService = ProductGroupServiceImpl.getInstance();
        List<ProductGroupDTO> productGroupByListTitle = productGroupService.findProductGroupByListTitle(productGroupList);
        productDTO.setProductGroup(productGroupByListTitle);
        productDTO.setCount(Integer.parseInt(count));
        ProductDTO product = productService.createProduct(productDTO);
        if (product == null) {
        }
        request.setAttribute("addNewProduct", true);
        request.setAttribute("product", productDTO);
        request.getRequestDispatcher("/resources/jsp/admin/settingProduct.jsp").forward(request, response);
    }
}
