package com.log.market.controller;

import com.log.market.dto.ProductDTO;
import com.log.market.dto.ProductGroupDTO;
import com.log.market.service.api.ProductGroupService;
import com.log.market.service.api.ProductService;
import com.log.market.service.impl.ProductGroupServiceImpl;
import com.log.market.service.impl.ProductServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by User on 17.12.2016.
 */
public class UpdateProductByAdminServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("updateProductGroupByA", false);
        request.getRequestDispatcher("/resources/jsp/admin/settingProduct.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Integer id = Integer.parseInt(request.getParameter("id"));
        String title = request.getParameter("title");
        String description = request.getParameter("description");
        String price = request.getParameter("price");
        Integer count = Integer.parseInt(request.getParameter("count"));
        String productGroup = request.getParameter("group");
        String[] pr = productGroup.split(",");
        List<String> productGroupList = new LinkedList<String>();
        for (String s : pr) {
            productGroupList.add(s);
        }
        ProductGroupService productGroupService = ProductGroupServiceImpl.getInstance();
        List<ProductGroupDTO> productGroupDTOs = productGroupService.findProductGroupByListTitle(productGroupList);
        ProductService productService = ProductServiceImpl.getInstance();
        ProductDTO productDTO = new ProductDTO();
        productDTO.setId(id);
        productDTO.setTitle(title);
        productDTO.setDescription(description);
        productDTO.setPrice(price);
        productDTO.setCount(count);
        productDTO.setProductGroup(productGroupDTOs);
        ProductDTO product = productService.updateProduct(productDTO);
        List<ProductGroupDTO> productGroup1 = product.getProductGroup();
        if (productGroup != null) {
            request.getSession().setAttribute("update", product);
            request.setAttribute("updateProduct", true);
            request.getSession().setAttribute("list", productGroup1);
            request.getRequestDispatcher("/resources/jsp/admin/settingProduct.jsp").forward(request, response);
        } else {
            request.setAttribute("notUpdateProduct", true);
            request.setAttribute("allProductGroupsShow", false);
            request.getRequestDispatcher("/resources/jsp/admin/settingProduct.jsp").forward(request, response);
        }
    }
}
