package com.log.market.controller;

import com.log.market.dto.ProductDTO;
import com.log.market.dto.ProductGroupDTO;
import com.log.market.service.api.ProductGroupService;
import com.log.market.service.impl.ProductGroupServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by User on 06.12.2016.
 */
public class HomePageServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ProductGroupService productGroupService = ProductGroupServiceImpl.getInstance();
        List<ProductGroupDTO> productGroupDTOList = productGroupService.findAllProductGroups();
        if (productGroupDTOList != null) {
            request.setAttribute("productGroups", productGroupDTOList);
        }

        request.setAttribute("findProductOnHomePage", false);
        request.setAttribute("notFindProductOnHomePage", false);
        request.getRequestDispatcher("/resources/jsp/homePage.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String searchProduct = request.getParameter("productGroup");
        ProductGroupService productGroupService = ProductGroupServiceImpl.getInstance();
        ProductGroupDTO productGroupDTO = productGroupService.findProductGroupByTitle(searchProduct);
        List<ProductDTO> products = productGroupService.findProducts(productGroupDTO.getId());
        if (products != null) {
            List<ProductGroupDTO> productGroupDTOList = productGroupService.findAllProductGroups();
            if (productGroupDTOList != null) {
                request.setAttribute("productGroups", productGroupDTOList);
            }
            request.setAttribute("productByProductGroupOnHomePage", products);
            request.setAttribute("findProductOnHomePage", true);
            request.getRequestDispatcher("/resources/jsp/homePage.jsp").forward(request, response);
        } else {
            List<ProductGroupDTO> productGroupDTOList = productGroupService.findAllProductGroups();
            if (productGroupDTOList != null) {
                request.setAttribute("productGroups", productGroupDTOList);
            }
            request.setAttribute("notFindProductOnHomePage", true);
            request.getRequestDispatcher("/resources/jsp/homePage.jsp").forward(request, response);
        }
    }
}
