package com.log.market.controller;

import com.log.market.dto.ProductDTO;
import com.log.market.dto.ProductGroupDTO;
import com.log.market.service.api.ProductGroupService;
import com.log.market.service.impl.ProductGroupServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by User on 17.12.2016.
 */
public class FindProductByProductGroupSettingProductServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getSession().setAttribute("findProductByProductGroupSetting", false);
        request.setAttribute("notFindProductByProductGroupByAdmin", false);
        request.getRequestDispatcher("/resources/jsp/admin/settingProduct.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String title = request.getParameter("findByProductGroupSetting");
        ProductGroupService productGroupService = ProductGroupServiceImpl.getInstance();
        ProductGroupDTO productGroupDTO = productGroupService.findProductGroupByTitle(title);
        List<ProductDTO> products = productGroupService.findProducts(productGroupDTO.getId());
        if (products != null) {
            request.setAttribute("productByProductGroupSetting", products);
            request.setAttribute("findProductByProductGroupSetting", true);
            request.getRequestDispatcher("/resources/jsp/admin/settingProduct.jsp").forward(request, response);
        } else {
            request.setAttribute("notFindProductByProductGroupSetting", true);
            request.getRequestDispatcher("/resources/jsp/admin/settingProduct.jsp").forward(request, response);
        }
    }
}