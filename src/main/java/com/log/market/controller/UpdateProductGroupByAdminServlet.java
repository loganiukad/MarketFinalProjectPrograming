package com.log.market.controller;

import com.log.market.dto.ProductGroupDTO;
import com.log.market.service.api.ProductGroupService;
import com.log.market.service.impl.ProductGroupServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by User on 08.12.2016.
 */
public class UpdateProductGroupByAdminServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ProductGroupService productGroupService = ProductGroupServiceImpl.getInstance();
        List<ProductGroupDTO> allProductGroups = productGroupService.findAllProductGroups();
        request.setAttribute("allProductGroupsShow", false);
        request.setAttribute("allProductGroups", allProductGroups);
        request.setAttribute("updateProductGroupByA", false);
        request.getRequestDispatcher("/resources/jsp/admin/settingProductGroup.jsp").forward(request, response);

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Integer id = Integer.parseInt(request.getParameter("id"));
        String title = request.getParameter("title");
        String description = request.getParameter("description");
        ProductGroupService productGroupService = ProductGroupServiceImpl.getInstance();
        ProductGroupDTO productGroupDTO = new ProductGroupDTO();
        productGroupDTO.setId(id);
        productGroupDTO.setTitle(title);
        productGroupDTO.setDescription(description);
        ProductGroupDTO productGroup = productGroupService.updateProductGroup(productGroupDTO);
        List<ProductGroupDTO> productGroupDTO1 = productGroupService.findAllProductGroups();
        if (productGroup != null) {
            request.setAttribute("update",productGroup);
            request.setAttribute("updateProductGroup", true);
            request.setAttribute("productGroupUpdate", productGroupDTO1);
            request.getRequestDispatcher("/resources/jsp/admin/settingProductGroup.jsp").forward(request, response);
        } else {
            request.setAttribute("notUpdateProductGroup", true);
            request.setAttribute("allProductGroupsShow", false);
            request.getRequestDispatcher("/resources/jsp/admin/settingProductGroup.jsp").forward(request, response);
        }
    }
}

















