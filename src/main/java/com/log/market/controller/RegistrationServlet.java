package com.log.market.controller;

import com.log.market.dto.ProductGroupDTO;
import com.log.market.dto.UserDTO;
import com.log.market.entity.UserRole;
import com.log.market.entity.UserSex;
import com.log.market.service.api.ProductGroupService;
import com.log.market.service.api.UserService;
import com.log.market.service.impl.ProductGroupServiceImpl;
import com.log.market.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by User on 25.11.2016.
 */
public class RegistrationServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ProductGroupService productGroupService = ProductGroupServiceImpl.getInstance();
        List<ProductGroupDTO> productGroupDTOList = productGroupService.findAllProductGroups();
        if (productGroupDTOList != null) {
            request.setAttribute("productGroups", productGroupDTOList);
        }
        request.setAttribute("registration", true);
        request.getRequestDispatcher("/resources/jsp/common.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String firstName = request.getParameter("firstName");
        String secondName = request.getParameter("secondName");
        String birthday = request.getParameter("birthday");
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        String email = request.getParameter("email");
        String sex = request.getParameter("sex");
        UserService userService = UserServiceImpl.getInstance();
        UserDTO userDTO = new UserDTO();
        userDTO.setFirstName(firstName);
        userDTO.setSecondName(secondName);
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        try {
            String s = birthday.toString();
            Date resDate = format.parse(s);
            userDTO.setBirthday(resDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        userDTO.setLogin(login);
        userDTO.setPassword(password);
        userDTO.setEmail(email);
        userDTO.setSex(UserSex.valueOf(sex));
        userDTO.setRole(UserRole.USER);
        userService.createUser(userDTO);
        HttpSession session = request.getSession();
        ProductGroupService productGroupService = ProductGroupServiceImpl.getInstance();
        List<ProductGroupDTO> productGroupDTOList = productGroupService.findAllProductGroups();
        if (productGroupDTOList != null) {
            request.setAttribute("productGroups", productGroupDTOList);
        }
        session.setAttribute("user", userDTO);
        request.getRequestDispatcher("/resources/jsp/homePage.jsp").forward(request, response);
    }

    public Date formatDate(String date) throws ParseException {
        String string = date.toString();
        DateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        Date resDate = format.parse(string);
        return resDate;

    }
}
