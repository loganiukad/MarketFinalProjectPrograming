package com.log.market.controller;

import com.log.market.dto.ProductDTO;
import com.log.market.service.api.ProductService;
import com.log.market.service.impl.ProductServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by User on 14.12.2016.
 */
public class FindProductOnCommonPageServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("findProductCommonPage", false);
        request.setAttribute("notFindProductCommonPage", false);
        request.getRequestDispatcher("/resources/jsp/common.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String title = request.getParameter("search");
        ProductService productService = ProductServiceImpl.getInstance();
        ProductDTO productDTO = productService.findByTitle(title);
        if (productDTO != null) {
            request.setAttribute("productFindCommonPage", productDTO);
            request.setAttribute("findProductCommonPage", true);
            request.getRequestDispatcher("/resources/jsp/common.jsp").forward(request, response);
        } else {
            request.setAttribute("notFindProductCommonPage", true);
            request.getRequestDispatcher("/resources/jsp/common.jsp").forward(request, response);
        }

    }
}
