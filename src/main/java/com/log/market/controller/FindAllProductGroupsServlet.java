package com.log.market.controller;

import com.log.market.dto.ProductGroupDTO;
import com.log.market.service.api.ProductGroupService;
import com.log.market.service.impl.ProductGroupServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by User on 02.12.2016.
 */
public class FindAllProductGroupsServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/resources/jsp/findAllProductGroups.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ProductGroupService productGroupService = ProductGroupServiceImpl.getInstance();
        List<ProductGroupDTO> productGroupDTOList = productGroupService.findAllProductGroups();
        request.setAttribute("productGroups", productGroupDTOList);
        request.getRequestDispatcher("/resources/jsp/homePage.jsp").forward(request, response);
    }
}
