package com.log.market.controller;

import com.log.market.dto.ProductDTO;
import com.log.market.dto.ProductGroupDTO;
import com.log.market.service.api.ProductService;
import com.log.market.service.impl.ProductServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by User on 19.12.2016.
 */
public class FindProductByAdminSettingServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/resources/jsp/productSetting.jsp").forward(request, response);
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String title = request.getParameter("title");
        ProductService productService = ProductServiceImpl.getInstance();
        ProductDTO productDTO = productService.findByTitle(title);
        if (productDTO != null) {
            List<ProductGroupDTO> productGroup = productDTO.getProductGroup();
            for (ProductGroupDTO productGroupDTO : productGroup) {
                request.setAttribute("productGroup", productGroupDTO.toString());
            }
            request.getSession().setAttribute("productFindOnSetting", productDTO);
            request.setAttribute("findProduct", true);
            request.getRequestDispatcher("/resources/jsp/admin/settingProduct.jsp").forward(request, response);
        } else {
            request.setAttribute("notFindProduct", true);
            request.getRequestDispatcher("/resources/jsp/admin/settingProduct.jsp").forward(request, response);
        }
    }
}
