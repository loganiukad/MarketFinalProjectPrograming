package com.log.market.controller;

import com.log.market.dto.ProductGroupDTO;
import com.log.market.service.api.ProductGroupService;
import com.log.market.service.impl.ProductGroupServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by User on 07.12.2016.
 */
public class SettingProductGroupServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ProductGroupService productGroupService = ProductGroupServiceImpl.getInstance();
        List<ProductGroupDTO> allProductGroups = productGroupService.findAllProductGroups();
        request.setAttribute("allProductGroupsShow", false);
        request.setAttribute("allProductGroups", allProductGroups);
        request.setAttribute("addProductGroup", false);
        request.setAttribute("notAddProductGroup", false);
        request.getRequestDispatcher("/resources/jsp/admin/settingProductGroup.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String title = request.getParameter("title");
        String description = request.getParameter("description");
        ProductGroupService productGroupService = ProductGroupServiceImpl.getInstance();
        ProductGroupDTO productGroupDTO = new ProductGroupDTO();
        productGroupDTO.setTitle(title);
        productGroupDTO.setDescription(description);
        ProductGroupDTO productGroup = productGroupService.createProductGroup(productGroupDTO);
        List<ProductGroupDTO> productGroupDTO1 = productGroupService.findAllProductGroups();
        if (productGroup != null) {
            request.setAttribute("addProductGroup", true);
            request.setAttribute("productGroupAdd", productGroupDTO1);
            request.getRequestDispatcher("/resources/jsp/admin/settingProductGroup.jsp").forward(request, response);
        } else {
            request.setAttribute("notAddProductGroup", true);
            request.setAttribute("allProductGroupsShow", false);
            request.getRequestDispatcher("/resources/jsp/admin/settingProductGroup.jsp").forward(request, response);
        }
    }
}
