package com.log.market.controller;

import com.log.market.dto.ProductGroupDTO;
import com.log.market.dto.UserDTO;
import com.log.market.service.api.ProductGroupService;
import com.log.market.service.api.UserService;
import com.log.market.service.impl.ProductGroupServiceImpl;
import com.log.market.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Created by User on 20.12.2016.
 */
public class CommonPageServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ProductGroupService productGroupService = ProductGroupServiceImpl.getInstance();
        List<ProductGroupDTO> productGroupDTOList = productGroupService.findAllProductGroups();
        if (productGroupDTOList != null) {
            request.setAttribute("productGroups", productGroupDTOList);
            request.getRequestDispatcher("/resources/jsp/common.jsp").forward(request, response);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userName = request.getParameter("userName");
        String password = request.getParameter("password");
        UserService userService = UserServiceImpl.getInstance();
        UserDTO user = userService.findByLoginAndPassword(userName, password);
        HttpSession session = request.getSession();
        if (user != null) {
            session.setAttribute("user", user);
            request.getRequestDispatcher("/resources/jsp/homePage.jsp").forward(request, response);
        } else {
            request.setAttribute("notAuth", true);
            request.getRequestDispatcher("/resources/jsp/common.jsp").forward(request, response);
        }
    }
}
