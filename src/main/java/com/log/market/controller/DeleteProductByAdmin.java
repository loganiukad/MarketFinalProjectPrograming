package com.log.market.controller;

import com.log.market.service.api.ProductGroupService;
import com.log.market.service.impl.ProductGroupServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by User on 17.12.2016.
 */
public class DeleteProductByAdmin extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/resources/jsp/admin/settingProductGroup.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Integer id = Integer.parseInt(request.getParameter("id"));
        ProductGroupService productGroupService = ProductGroupServiceImpl.getInstance();
        productGroupService.deleteProductGroup(id);
        request.setAttribute("deleteProduct", true);
        request.getRequestDispatcher("/resources/jsp/admin/settingProductGroup.jsp").forward(request, response);
    }
}
