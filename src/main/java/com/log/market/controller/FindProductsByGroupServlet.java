package com.log.market.controller;

import com.log.market.dto.ProductDTO;
import com.log.market.dto.ProductGroupDTO;
import com.log.market.service.api.ProductGroupService;
import com.log.market.service.impl.ProductGroupServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by User on 09.12.2016.
 */
public class FindProductsByGroupServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ProductGroupService productGroupService = ProductGroupServiceImpl.getInstance();
        List<ProductGroupDTO> productGroupDTOList = productGroupService.findAllProductGroups();
        request.setAttribute("productGroups", productGroupDTOList);
        request.getSession().setAttribute("findProductByProductGroup", false);
        request.setAttribute("notFindProductByProductGroup", false);
        request.getRequestDispatcher("/resources/jsp/common.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String title = request.getParameter("productGroup");
        ProductGroupService productGroupService = ProductGroupServiceImpl.getInstance();
        List<ProductDTO> products = null;
        ProductGroupDTO productGroupDTO = productGroupService.findProductGroupByTitle(title);
        if (productGroupDTO != null) {
            products = productGroupService.findProducts(productGroupDTO.getId());
        }
        if (products != null) {
            List<ProductGroupDTO> productGroupDTOList = productGroupService.findAllProductGroups();
            request.setAttribute("productGroups", productGroupDTOList);
            request.setAttribute("productByProductGroup", products);
            request.setAttribute("findProductByProductGroup", true);
            request.getRequestDispatcher("/resources/jsp/common.jsp").forward(request, response);
        } else {
            List<ProductGroupDTO> productGroupDTOList = productGroupService.findAllProductGroups();
            request.setAttribute("productGroups", productGroupDTOList);
            request.setAttribute("notFindProductByProductGroup", true);
            request.getRequestDispatcher("/resources/jsp/common.jsp").forward(request, response);
        }
    }
}

