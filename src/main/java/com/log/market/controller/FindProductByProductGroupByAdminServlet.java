package com.log.market.controller;

import com.log.market.dto.ProductDTO;
import com.log.market.dto.ProductGroupDTO;
import com.log.market.service.api.ProductGroupService;
import com.log.market.service.impl.ProductGroupServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by User on 15.12.2016.
 */
public class FindProductByProductGroupByAdminServlet extends HttpServlet{

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getSession().setAttribute("findProductByProductGroupByAdmin", false);
        request.setAttribute("notFindProductByProductGroupByAdmin", false);
        request.getRequestDispatcher("/resources/jsp/admin/adminPage.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String title = request.getParameter("findProductByGroupByAdmin");
        ProductGroupService productGroupService = ProductGroupServiceImpl.getInstance();
        ProductGroupDTO productGroupDTO = productGroupService.findProductGroupByTitle(title);
        List<ProductDTO> products = productGroupService.findProducts(productGroupDTO.getId());
        if (products != null) {
            request.setAttribute("productByProductGroupByAdmin", products);
            request.setAttribute("findProductByProductGroupByAdmin", true);
            request.getRequestDispatcher("/resources/jsp/admin/adminPage.jsp").forward(request, response);
        } else {
            request.setAttribute("notFindProductByProductGroupByAdmin", true);
            request.getRequestDispatcher("/resources/jsp/admin/adminPage.jsp").forward(request, response);
        }
    }
}


