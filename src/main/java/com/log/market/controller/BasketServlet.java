package com.log.market.controller;

import com.log.market.dto.ProductDTO;
import com.log.market.dto.ProductGroupDTO;
import com.log.market.service.api.ProductGroupService;
import com.log.market.service.api.ProductService;
import com.log.market.service.impl.ProductGroupServiceImpl;
import com.log.market.service.impl.ProductServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by User on 16.12.2016.
 */
public class BasketServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        List<ProductDTO> addProductToBasket = (LinkedList) session.getAttribute("addProductToBasket");
        if (addProductToBasket == null) {
            request.setAttribute("basketIsEmpty", false);
        } else {
            request.setAttribute("addToBasket", true);
        }
        request.getRequestDispatcher("/resources/jsp/basket.jsp").forward(request, response);
        ProductGroupService productGroupService = ProductGroupServiceImpl.getInstance();
        List<ProductGroupDTO> productGroupDTOList = productGroupService.findAllProductGroups();
        request.getSession().setAttribute("productGroups", productGroupDTOList);
        request.getRequestDispatcher("/resources/jsp/homePage.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        List<ProductDTO> addProductToBasket = (LinkedList) session.getAttribute("addProductToBasket");
        if (addProductToBasket == null) {
            addProductToBasket = new LinkedList<ProductDTO>();
        }
        String title = request.getParameter("title");
        ProductService productService = ProductServiceImpl.getInstance();
        ProductGroupService productGroupService = ProductGroupServiceImpl.getInstance();
        ProductDTO productBasket = productService.addToBasket(title);
        addProductToBasket.add(productBasket);
        request.setAttribute("addToBasket", true);
        List<ProductGroupDTO> productGroupDTOList = productGroupService.findAllProductGroups();
        if (productGroupDTOList != null) {
            request.setAttribute("productGroups", productGroupDTOList);
        }
        request.getSession().setAttribute("addProductToBasket", addProductToBasket);
        request.getRequestDispatcher("/resources/jsp/homePage.jsp").forward(request, response);
    }
}
