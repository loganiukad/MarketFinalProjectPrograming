package com.log.market.controller;

import com.log.market.dto.ProductDTO;
import com.log.market.dto.ProductGroupDTO;
import com.log.market.service.api.ProductGroupService;
import com.log.market.service.api.ProductService;
import com.log.market.service.impl.ProductGroupServiceImpl;
import com.log.market.service.impl.ProductServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by User on 28.11.2016.
 */
public class FindProductServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        ProductGroupService productGroupService = ProductGroupServiceImpl.getInstance();
        List<ProductGroupDTO> productGroupDTOList = productGroupService.findAllProductGroups();
        if (productGroupDTOList != null) {
            request.setAttribute("productGroups", productGroupDTOList);
        }
        request.setAttribute("findProduct", false);
        request.setAttribute("notFindProduct", false);
        request.getRequestDispatcher("/resources/jsp/homePage.jsp").forward(request, response);
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String title = request.getParameter("oneProductSearch");
        ProductService productService = ProductServiceImpl.getInstance();
        ProductGroupService productGroupService = ProductGroupServiceImpl.getInstance();
        ProductDTO productDTO = productService.findByTitle(title);
        if (productDTO != null) {
            List<ProductGroupDTO> productGroupDTOList = productGroupService.findAllProductGroups();
            if (productGroupDTOList != null) {
                request.setAttribute("productGroups", productGroupDTOList);
            }
            request.setAttribute("productFind", productDTO);
            request.setAttribute("findProduct", true);
            request.getRequestDispatcher("/resources/jsp/homePage.jsp").forward(request, response);
        } else {
            List<ProductGroupDTO> productGroupDTOList = productGroupService.findAllProductGroups();
            if (productGroupDTOList != null) {
                request.setAttribute("productGroups", productGroupDTOList);
            }
            request.setAttribute("notFindProduct", true);
            request.getRequestDispatcher("/resources/jsp/homePage.jsp").forward(request, response);
        }
    }
}
