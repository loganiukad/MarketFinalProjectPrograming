package com.log.market.controller;

import com.log.market.dto.ProductGroupDTO;
import com.log.market.service.api.ProductGroupService;
import com.log.market.service.impl.ProductGroupServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by User on 06.12.2016.
 */
public class AdminPageServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/resources/jsp/admin/adminPage.jsp").forward(request, response);
        ProductGroupService productGroupService = ProductGroupServiceImpl.getInstance();
        List<ProductGroupDTO> productGroupDTOList = productGroupService.findAllProductGroups();
        request.getSession().setAttribute("productGroups", productGroupDTOList);
        request.setAttribute("findProduct",false);
        request.setAttribute("notFindProduct", false);
        request.getRequestDispatcher("/resources/jsp/adminPage.jsp").forward(request, response);
    }
}
