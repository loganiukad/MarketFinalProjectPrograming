package com.log.market.helper;

import com.log.market.dto.ProductDTO;
import com.log.market.dto.ProductGroupDTO;
import com.log.market.dto.TransactionDTO;
import com.log.market.dto.UserDTO;
import com.log.market.entity.Product;
import com.log.market.entity.ProductGroup;
import com.log.market.entity.Transaction;
import com.log.market.entity.User;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by User on 10.10.2016.
 */
public class Transformer {
    private static volatile Transformer instance;

    private Transformer() {
    }

    public static Transformer getInstance() {
        if (instance == null) {
            synchronized (Transformer.class) {
                if (instance == null) {
                    instance = new Transformer();
                }
            }
        }
        return instance;
    }

    public static UserDTO transformUsersToUsersDTO(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setFirstName(user.getFirstName());
        userDTO.setLogin(user.getLogin());
        userDTO.setPassword(user.getPassword());
        userDTO.setBirthday(user.getBirthday());
        userDTO.setEmail(user.getEmail());
        userDTO.setRole(user.getRole());
        userDTO.setSecondName(user.getSecondName());
        userDTO.setSex(user.getSex());
        return userDTO;
    }

    public static User transformUserDTOToUser(UserDTO userDTO) {
        User user = new User();
        user.setId(userDTO.getId());
        user.setLogin(userDTO.getLogin());
        user.setFirstName(userDTO.getFirstName());
        user.setPassword(userDTO.getPassword());
        user.setSex(userDTO.getSex());
        user.setSecondName(userDTO.getSecondName());
        user.setRole(userDTO.getRole());
        user.setEmail(userDTO.getEmail());
        user.setBirthday(userDTO.getBerthday());
        return user;
    }

    public static List<User> transformListUserDTOToListUser(LinkedList<UserDTO> userDTOs) {
        List<User> users = new LinkedList<User>();
        for (UserDTO userDTO : userDTOs) {
            User user = transformUserDTOToUser(userDTO);
            users.add(user);
        }
        return users;
    }


    public static ProductDTO transformProductToProductDTO(Product product) {
        ProductDTO productDTO = new ProductDTO();
        productDTO.setId(product.getId());
        productDTO.setCount(product.getCount());
        productDTO.setDescription(product.getDescription());
        productDTO.setTitle(product.getTitle());
        productDTO.setPrice(product.getPrice());
        productDTO.setProductGroup(transformListProductGroupToListProductGroupDTO(product.getProductGroup()));

        return productDTO;
    }

    public static Product transformProductDTOToProduct(ProductDTO productDTO) {
        Product product = new Product();
        product.setId(productDTO.getId());
        product.setCount(productDTO.getCount());
        product.setDescription(productDTO.getDescription());
        product.setTitle(productDTO.getTitle());
        product.setPrice(productDTO.getPrice());
        product.setProductGroup(transformListDTOProductGroupToListProductGroup(productDTO.getProductGroup()));
        return product;
    }

    public static List<Product> transformListDTOProductToListProduct(List<ProductDTO> productDTO) {
        List<Product> products = new LinkedList<Product>();
        for (ProductDTO pr : productDTO) {
            Product product = transformProductDTOToProduct(pr);
            products.add(product);

        }
        return products;
    }

    public static List<ProductDTO> transformListProductToListProductDTO(List<Product> product) {
        List<ProductDTO> productDTOs = new LinkedList<ProductDTO>();
        for (Product pr : product) {
            ProductDTO productDTO = transformProductToProductDTO(pr);
            productDTOs.add(productDTO);
        }
        return productDTOs;
    }

    public static List<ProductGroup> transformListDTOProductGroupToListProductGroup(List<ProductGroupDTO> productGroupDTO) {
        List<ProductGroup> productGroups = new LinkedList<ProductGroup>();
        for (ProductGroupDTO prGroup : productGroupDTO) {
            ProductGroup productGroup = transformProductGroupDTOToProductGroup(prGroup);
            productGroups.add(productGroup);
        }
        return productGroups;
    }

    public static List<ProductGroupDTO> transformListProductGroupToListProductGroupDTO(List<ProductGroup> productGroup) {
        List<ProductGroupDTO> productGroupDTOs = new LinkedList<ProductGroupDTO>();
        if (productGroup != null){
            for (ProductGroup group : productGroup) {
                ProductGroupDTO productGroupDTO = transformProductGroupToProductGroupDTO(group);
                productGroupDTOs.add(productGroupDTO);
            }
        }
        return productGroupDTOs;
    }

    public static TransactionDTO transformTransactionToTransactionDTO(Transaction transaction) {
        TransactionDTO transactionDTO = new TransactionDTO();
        transactionDTO.setId(transaction.getId());
        transactionDTO.setDate(transaction.getDate());
        transactionDTO.setProduct(transaction.getProduct());
        transactionDTO.setProductCount(transaction.getProductCount());
        transactionDTO.setProductPrice(transaction.getProductPrice());
        return transactionDTO;
    }

    public static Transaction transformTransactionDTOToTransaction(TransactionDTO transactionDTO) {
        Transaction transaction = new Transaction();
        transaction.setDate(transactionDTO.getDate());
        transaction.setId(transactionDTO.getId());
        transaction.setProduct(transactionDTO.getProduct());
        transaction.setProductPrice(transactionDTO.getProductPrice());
        transaction.setProductCount(transactionDTO.getProductCount());
        return transaction;
    }

    public static ProductGroupDTO transformProductGroupToProductGroupDTO(ProductGroup productGroup) {
        ProductGroupDTO productGroupDTO = new ProductGroupDTO();
        productGroupDTO.setId(productGroup.getId());
        productGroupDTO.setTitle(productGroup.getTitle());
        productGroupDTO.setDescription(productGroup.getDescription());
        return productGroupDTO;
    }

    public static ProductGroup transformProductGroupDTOToProductGroup(ProductGroupDTO productGroupDTO) {
        ProductGroup productGroup = new ProductGroup();
        productGroup.setId(productGroupDTO.getId());
        productGroup.setTitle(productGroupDTO.getTitle());
        productGroup.setDescription(productGroupDTO.getDescription());
        productGroup.setProducts(productGroupDTO.getListProductDTO());
        return productGroup;
    }
}
