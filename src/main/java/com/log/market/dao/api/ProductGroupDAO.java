package com.log.market.dao.api;

import com.log.market.entity.Product;
import com.log.market.entity.ProductGroup;

import java.util.List;

/**
 * Created by User on 10.10.2016.
 */
public interface ProductGroupDAO {

    ProductGroup findProductGroupById(Integer id);

    ProductGroup create(ProductGroup productGroup);

    ProductGroup update(ProductGroup productGroup);

    boolean delete(Integer id);

    List<Product> findProducts(Integer id);

    List<ProductGroup> findAllProductGroups();

    ProductGroup findProductGroupByTitle(String title);

    List<ProductGroup> findProductGroupByListTitle(List<String> title);

}
