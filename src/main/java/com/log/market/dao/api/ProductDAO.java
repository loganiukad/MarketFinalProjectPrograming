package com.log.market.dao.api;

import com.log.market.entity.Product;
import com.log.market.entity.ProductGroup;

import java.util.List;

/**
 * Created by User on 10.10.2016.
 */
public interface ProductDAO {

    Product findProductById(Integer id);

    Product create(Product product);

    Product update(Product product);

    boolean delete(Integer id);

    List<ProductGroup> findProductGroups(Integer id);

    Product findProductByTitle(String title);

    boolean isExistProduct (String title);

    Product addToBasket (String title);

    Product deleteFromBasket(String title);

    boolean  returnCount(Product product);

}
