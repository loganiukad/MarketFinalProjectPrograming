package com.log.market.dao.api;

import com.log.market.entity.Transaction;

import java.util.List;

/**
 * Created by User on 10.10.2016.
 */
public interface TransactionDAO {

    Transaction create(Transaction transaction);

    List<Transaction> findAllByUserId(Integer id);

    Transaction findTransactionById(Integer id);

    boolean deleteTransaction(Integer id);

    Transaction updateTransaction(Transaction transaction);
}
