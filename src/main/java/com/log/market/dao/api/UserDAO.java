package com.log.market.dao.api;

import com.log.market.entity.Product;
import com.log.market.entity.User;

import java.util.List;

/**
 * Created by User on 10.10.2016.
 */
public interface UserDAO {

    User findByLoginAndPassword(String login, String password );
    User findByLoginAndEmail(String login, String email);
    User create(User user);
    boolean delete(Integer id);
    User update(User user);
    boolean isLoginExist(String login);
    List<Product> findBasketProduct(Integer id);
}
