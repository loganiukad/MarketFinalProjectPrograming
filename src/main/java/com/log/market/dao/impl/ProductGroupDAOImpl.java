package com.log.market.dao.impl;

import com.log.market.dao.api.ProductDAO;
import com.log.market.dao.api.ProductGroupDAO;
import com.log.market.datasource.DataSource;
import com.log.market.entity.Product;
import com.log.market.entity.ProductGroup;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by User on 10.10.2016.
 */
public class ProductGroupDAOImpl implements ProductGroupDAO {

    private static DataSource dataSource;

    private String findProductGroupById = "SELECT * FROM test.productgroups WHERE id = ?";
    private String findProductGroupByTitle = "SELECT * FROM test.productgroups WHERE title = ?";
    private String findProductGroupByListTitle = "SELECT * FROM test.productgroups WHERE title = ?";
    private String createProductGroups = "INSERT INTO test.productgroups ( title, description) VALUES (?,?)";
    private String updateProductGroups = "UPDATE test.productgroups SET title = ?, description = ? WHERE id = ?";
    private String findAllProductGroups = "SELECT * FROM test.productgroups";
    private String deleteProductGroup = "DELETE FROM test.productgroups WHERE id = ?";
    private String findProduct = "SELECT product FROM test.productgrouptoproduct WHERE productGroupID = ?";
    private static volatile ProductGroupDAO instance;

    private ProductGroupDAOImpl() {
    }

    public static ProductGroupDAO getInstance() {
        if (instance == null) {
            synchronized (ProductGroupDAOImpl.class) {
                if (instance == null) {
                    instance = new ProductGroupDAOImpl();
                    dataSource = DataSource.getInstance();
                }
            }
        }
        return instance;
    }

    @Override
    public List<ProductGroup> findAllProductGroups() {
        List<ProductGroup> productGroupList = new LinkedList<ProductGroup>();
        ProductGroup productGroup1 = null;
        Connection connection = dataSource.createConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(findAllProductGroups);
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                while (resultSet.next()) {
                    productGroup1 = new ProductGroup();
                    productGroup1.setId(resultSet.getInt("id"));
                    productGroup1.setTitle(resultSet.getString("title"));
                    productGroup1.setDescription(resultSet.getString("description"));
                    productGroupList.add(productGroup1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return productGroupList;
    }

    @Override
    public ProductGroup findProductGroupByTitle(String title) {
        ProductGroup productGroup = null;
        Connection connection = dataSource.createConnection();
        try {
           PreparedStatement preparedStatement = connection.prepareStatement(findProductGroupByTitle);
            preparedStatement.setString(1, title);
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                    productGroup = new ProductGroup();
                    productGroup.setId(resultSet.getInt("id"));
                    productGroup.setTitle(resultSet.getString("title"));
                    productGroup.setDescription(resultSet.getString("description"));
          }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return productGroup;
    }

    @Override
    public List<ProductGroup> findProductGroupByListTitle(List<String> title) {
        List<ProductGroup> productGroupList = new LinkedList<ProductGroup>();
        ProductGroup productGroup = null;
        Connection connection = dataSource.createConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(findProductGroupByTitle);
                for (String s : title) {
                preparedStatement.setString(1, s);
                ResultSet resultSet = preparedStatement.executeQuery();
                if(resultSet.next()){
                    while (resultSet.next()) {
                        productGroup = new ProductGroup();
                        productGroup.setId(resultSet.getInt("id"));
                        productGroup.setTitle(resultSet.getString("title"));
                        productGroup.setDescription(resultSet.getString("description"));
                        productGroupList.add(productGroup);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
       return productGroupList;
    }

    @Override
    public ProductGroup findProductGroupById(Integer id) {
        ProductGroup productGroup = null;
        Connection connection = dataSource.createConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(findProductGroupById);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                productGroup = new ProductGroup();
                productGroup.setId(resultSet.getInt("id"));
                productGroup.setTitle(resultSet.getString("title"));
                productGroup.setDescription(resultSet.getString("description"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return productGroup;
    }

    public ProductGroup create(ProductGroup productGroup) {

        ProductGroup productGroupsCreate = null;
        Connection connection = dataSource.createConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(createProductGroups, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, productGroup.getTitle());
            preparedStatement.setString(2, productGroup.getDescription());
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                productGroup.setId(resultSet.getInt(1));
                productGroupsCreate = productGroup;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return productGroupsCreate;
    }

    public ProductGroup update(ProductGroup productGroup) {
        ProductGroup updateProductGroup = null;
        Connection connection = dataSource.createConnection();
        try {
            ProductGroup productGroupByTitle = findProductGroupByTitle(productGroup.getTitle());
            PreparedStatement preparedStatement = connection.prepareStatement(updateProductGroups, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, productGroup.getTitle());
            preparedStatement.setString(2, productGroup.getDescription());
            preparedStatement.setInt(3, productGroup.getId());
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                productGroup.setId(resultSet.getInt(1));
                updateProductGroup = productGroup;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return productGroup;
    }

    public boolean delete(Integer id) {
        boolean resultSet = false;
        Connection connection = dataSource.createConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(deleteProductGroup);
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultSet;
    }

    public List<Product> findProducts(Integer id) {
        List<Product> resProduct = null;
        Connection connection = dataSource.createConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(findProduct);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            resProduct = new LinkedList<Product>();
            if(resultSet.next()){
                while (resultSet.next()) {
                    ProductDAO productDAO = ProductDAOImpl.getInstance();
                    Product product = productDAO.findProductByTitle(resultSet.getString("product"));
                    resProduct.add(product);
                }
                if (resProduct.size() == 0) {
                    resProduct = null;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resProduct;
    }
}
