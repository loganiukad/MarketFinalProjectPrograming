package com.log.market.dao.impl;

import com.log.market.dao.api.UserDAO;
import com.log.market.datasource.DataSource;
import com.log.market.entity.Product;
import com.log.market.entity.User;
import com.log.market.entity.UserRole;
import com.log.market.entity.UserSex;

import java.sql.*;
import java.util.List;

/**
 * Created by User on 10.10.2016.
 */
public class UserDAOImpl implements UserDAO {

    private static DataSource dataSource;

    private String findByLoginAndPassword = "SELECT * FROM test.users WHERE login = ? AND password = ?";
    private String findByLoginAndEmail = "SELECT * FROM test.users WHERE login = ? AND email = ?";
    private String createUser = "INSERT INTO test.users ( firstName, secondName, birthday, login, password, email, sex, role) VALUES ( ?, ?,?,?,?,?,?,?)";
    private String findUserByLogin = "SELECT * FROM test.users WHERE login = ?";
    private String deleteUser = "DELETE FROM users WHERE id = ?";
    private String updateUser = "UPDATE test.users SET firstName = ?, secondName = ?, password = ?, email = ?,WHERE id = ?";

    private static volatile UserDAO instance;


    private UserDAOImpl() {
    }

    public static UserDAO getInstance() {
        if (instance == null) {
            synchronized (UserDAOImpl.class) {
                if (instance == null) {
                    instance = new UserDAOImpl();
                    dataSource = DataSource.getInstance();
                }
            }
        }
        return instance;
    }

    public User findByLoginAndPassword(String login, String password) {
        User user = null;
        Connection connection = dataSource.createConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(findByLoginAndPassword);
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, password);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                user = new User();
                user.setId(resultSet.getInt("id"));
                user.setFirstName(resultSet.getString("firstName"));
                user.setSecondName(resultSet.getString("secondName"));
                user.setBirthday(resultSet.getDate("birthday"));
                user.setLogin(resultSet.getString("login"));
                user.setPassword(resultSet.getString("password"));
                user.setEmail(resultSet.getString("email"));
                user.setSex(UserSex.findUserSexByNumber(resultSet.getInt("sex")));
                user.setRole(UserRole.findUserRoleByNumber(resultSet.getInt("role")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    public User findByLoginAndEmail(String login, String email) {
        User user = null;
        Connection connection = dataSource.createConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(findByLoginAndEmail);
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, email);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                user = new User();
                user.setId(resultSet.getInt("id"));
                user.setFirstName(resultSet.getString("firstName"));
                user.setSecondName(resultSet.getString("secondName"));
                user.setBirthday(resultSet.getDate("birthday"));
                user.setLogin(resultSet.getString("login"));
                user.setPassword(resultSet.getString("password"));
                user.setEmail(resultSet.getString("email"));
                user.setSex(UserSex.findUserSexByNumber(resultSet.getInt("sex")));
                user.setRole(UserRole.findUserRoleByNumber(resultSet.getInt("role")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    public User create(User user) {

        User userCreate = null;
        Connection connection = dataSource.createConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(createUser, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, user.getFirstName());
            preparedStatement.setString(2, user.getSecondName());
            java.sql.Date sDate = new java.sql.Date(user.getBirthday().getTime());
            preparedStatement.setDate(3, sDate);
            preparedStatement.setString(4, user.getLogin());
            preparedStatement.setString(5, user.getPassword());
            preparedStatement.setString(6, user.getEmail());
            preparedStatement.setInt(7, user.getSex().ordinal());
            preparedStatement.setInt(8, user.getRole().ordinal());
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                user.setId(resultSet.getInt(1));
                userCreate = user;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userCreate;
    }

    public boolean delete(Integer id) {
        boolean resultSet = false;
        Connection connection = dataSource.createConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(deleteUser);
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultSet;
    }

    public User update(User user) {
        User userUpdate = null;
        Connection connection = dataSource.createConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(updateUser, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, user.getFirstName());
            preparedStatement.setString(2, user.getSecondName());
            preparedStatement.setString(4, user.getPassword());
            preparedStatement.setString(5, user.getEmail());
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                user.setId(resultSet.getInt(1));
                userUpdate = user;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userUpdate;
    }

    public boolean isLoginExist(String login) {
        boolean isExist = true;
        Connection connection = dataSource.createConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(findUserByLogin);
            preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();
            isExist = resultSet.next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isExist;
    }

    @Override
    public List<Product> findBasketProduct(Integer id) {
        List<Product> basketProductList = null;
        return null;
    }
}


