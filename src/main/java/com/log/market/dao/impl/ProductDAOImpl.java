package com.log.market.dao.impl;

import com.log.market.dao.api.ProductDAO;
import com.log.market.dao.api.ProductGroupDAO;
import com.log.market.datasource.DataSource;
import com.log.market.dto.ProductGroupDTO;
import com.log.market.entity.Product;
import com.log.market.entity.ProductGroup;
import com.log.market.helper.Transformer;
import com.log.market.service.api.ProductGroupService;
import com.log.market.service.impl.ProductGroupServiceImpl;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by User on 10.10.2016.
 */
public class ProductDAOImpl implements ProductDAO {

    private static DataSource dataSource;

    private String findProductById = "select * from product where id = ?";
    private String createProducts = "INSERT INTO test.products (title, description,price,count,productGroup) VALUES (?, ?,?,?,?)";
    private String updateProducts = "UPDATE test.products SET title = ?, description = ?, price = ?, count = ?, productGroup = ? WHERE id = ?";
    private String updateProductForBasket = "UPDATE test.products SET count = ? WHERE id = ?";
    private String deleteProducts = "DELETE FROM test.products WHERE id = ?";
    private String toProductGroup = "INSERT INTO test.productgrouptoproduct (productID,productGroupID,productGroup, product) VALUES (?,?,?,?)";
    private String countReturn = " UPDATE test.products SET count = ? WHERE id = ?";
    private String findProductByTitle = "SELECT * FROM test.products WHERE title = ?";
    private String findProductGroups = "SELECT productGroup FROM test.productgrouptoproduct WHERE productID = ?";
    private static volatile ProductDAO instance;

    private ProductDAOImpl() {
    }

    public static ProductDAO getInstance() {
        if (instance == null) {
            synchronized (ProductDAOImpl.class) {
                if (instance == null) {
                    instance = new ProductDAOImpl();
                    dataSource = DataSource.getInstance();
                }
            }
        }
        return instance;
    }

    @Override
    public Product findProductById(Integer id) {
        Product product = null;
        Connection connection = dataSource.createConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(findProductById);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                product = new Product();
                product.setId(resultSet.getInt("id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return product;
    }

    public Product create(Product product) {
        Product productCreate = null;
        Connection connection = dataSource.createConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(createProducts, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, product.getTitle());
            preparedStatement.setString(2, product.getDescription());
            preparedStatement.setString(3, product.getPrice());
            preparedStatement.setInt(4, product.getCount());
            List<ProductGroup> productGroup = product.getProductGroup();
            List<String> productGroupTitle = new LinkedList<String>();
            for (ProductGroup group : productGroup) {
                productGroupTitle.add(group.getTitle());
                preparedStatement.setString(5, group.getTitle());
                preparedStatement.addBatch();
            }
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                product.setId(resultSet.getInt(1));
                productCreate = product;
            }
            PreparedStatement preparedStatement1 = connection.prepareStatement(toProductGroup);
            List<ProductGroup> productGroup1 = product.getProductGroup();
            for (ProductGroup group1 : productGroup1) {
                preparedStatement1.setInt(1, product.getId());
                preparedStatement1.setInt(2, group1.getId());
                preparedStatement1.setString(3, group1.getTitle());
                preparedStatement1.setString(4, product.getTitle());
                preparedStatement1.addBatch();
            }
            preparedStatement1.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return productCreate;
    }


    public boolean returnCount(Product product) {
        boolean productReturn = false;
        Product res = null;
        Connection connection = dataSource.createConnection();
        try {
            Product productByTitle = findProductByTitle(product.getTitle());
            res = new Product();
            res.setId(productByTitle.getId());
            res.setPrice(productByTitle.getPrice());
            res.setTitle(productByTitle.getTitle());
            res.setDescription(productByTitle.getDescription());
            res.setCount(productByTitle.getCount());
            PreparedStatement preparedStatement1 = connection.prepareStatement(countReturn, Statement.RETURN_GENERATED_KEYS);
            preparedStatement1.setInt(1, res.getCount() + 1);
            preparedStatement1.setInt(2, res.getId());
            preparedStatement1.executeUpdate();
            productReturn = preparedStatement1.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return productReturn;
    }

    public Product update(Product product) {
        Product productUpdate = null;
        Connection connection = dataSource.createConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(updateProducts, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, product.getTitle());
            preparedStatement.setString(2, product.getDescription());
            preparedStatement.setString(3, product.getPrice());
            preparedStatement.setInt(4, product.getCount());
            List<ProductGroup> productGroup = product.getProductGroup();
            List<String> productGroupTitle = new LinkedList<String>();
            for (ProductGroup group : productGroup) {
                productGroupTitle.add(group.getTitle());
            }
            preparedStatement.setString(5, productGroupTitle.toString());
            preparedStatement.setInt(6, product.getId());
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            while (resultSet.next()) {
                product.setId(resultSet.getInt(1));
                String str = resultSet.getString("productGroup");
                String[] pr = str.split(",");
                List<String> productGroupList = new LinkedList<String>();
                for (String s : pr) {
                    productGroupList.add(s);
                }
                ProductGroupDAO productGroupDAO = ProductGroupDAOImpl.getInstance();
                List<ProductGroup> productGroupByListTitle = productGroupDAO.findProductGroupByListTitle(productGroupList);
                product.setProductGroup(productGroupByListTitle);
            }
            productUpdate = product;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return productUpdate;
    }

    public boolean delete(Integer id) {
        boolean resultSet = false;
        Connection connection = dataSource.createConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(deleteProducts);
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultSet;
    }

    public List<ProductGroup> findProductGroups(Integer id) {
        List<ProductGroup> resProductGroup = new LinkedList<ProductGroup>();
        Connection connection = dataSource.createConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(findProductGroups);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                ProductGroupDAO productGroupDAO = ProductGroupDAOImpl.getInstance();
                ProductGroup productGroup = productGroupDAO.findProductGroupByTitle(resultSet.getString("productGroup"));
                resProductGroup.add(productGroup);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resProductGroup;
    }


    @Override
    public Product addToBasket(String title) {
        Product res = null;
        Connection connection = dataSource.createConnection();
        try {
            Product productByTitle = findProductByTitle(title);
            res = new Product();
            res.setId(productByTitle.getId());
            res.setPrice(productByTitle.getPrice());
            res.setTitle(productByTitle.getTitle());
            res.setDescription(productByTitle.getDescription());
            res.setCount(productByTitle.getCount());
            PreparedStatement preparedStatement1 = connection.prepareStatement(updateProductForBasket, Statement.RETURN_GENERATED_KEYS);
            preparedStatement1.setInt(1, res.getCount() - 1);
            preparedStatement1.setInt(2, res.getId());
            preparedStatement1.executeUpdate();
            ResultSet resultSet = preparedStatement1.getGeneratedKeys();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public Product deleteFromBasket(String title) {
        Product res = null;
        Connection connection = dataSource.createConnection();
        try {
            Product productByTitle = findProductByTitle(title);
            res = new Product();
            res.setId(productByTitle.getId());
            res.setCount(productByTitle.getCount());
            PreparedStatement preparedStatement = connection.prepareStatement(countReturn, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, res.getCount() + 1);
            preparedStatement.setInt(2, res.getId());
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }


    @Override
    public Product findProductByTitle(String title) {
        Product res = null;
        Connection connection = dataSource.createConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(findProductByTitle);
            preparedStatement.setString(1, title);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                res = new Product();
                res.setId(resultSet.getInt("id"));
                res.setTitle(resultSet.getString("title"));
                res.setDescription(resultSet.getString("description"));
                res.setCount(resultSet.getInt("count"));
                res.setPrice(resultSet.getString("price"));
                String groups = resultSet.getString("productGroup");
                String[] groupsMas = groups.split(",");
                List<String> productGroupList = new LinkedList<String>();
                for (String groupsMa : groupsMas) {
                    productGroupList.add(groupsMa);
                }
                ProductGroupService productGroupService = ProductGroupServiceImpl.getInstance();
                List<ProductGroupDTO> productGroupByListTitle = productGroupService.findProductGroupByListTitle(productGroupList);
                res.setProductGroup(Transformer.transformListDTOProductGroupToListProductGroup(productGroupByListTitle));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public boolean isExistProduct(String title) {
        boolean isExist = true;
        Connection connection = dataSource.createConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(findProductByTitle);
            preparedStatement.setString(1, title);
            ResultSet resultSet = preparedStatement.executeQuery();
            isExist = resultSet.next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isExist;
    }
}
