package com.log.market.dao.impl;

import com.log.market.dao.api.TransactionDAO;
import com.log.market.datasource.DataSource;
import com.log.market.datasource.InMemoryDB;
import com.log.market.entity.Transaction;

import java.sql.*;
import java.util.List;

/**
 * Created by User on 10.10.2016.
 */
public class TransactionDAOImpl implements TransactionDAO {
    private static DataSource dataSource;
    private String createTransaction = "INSERT INTO test.transactions (product,productCount,productPrice,date) VALUES (?,?,?,?)";
    private String findTransactionById = "SELECT * FROM test.transactions WHERE id = ?";
    private String deleteTransaction = "DELETE FROM test.transactions WHERE id = ?;";
    private String updateTransaction = "UPDATE test.transactions SET productCount =  ? WHERE id = ?";
    private static volatile TransactionDAO instance;

    private TransactionDAOImpl() {
    }

    public static TransactionDAO getInstance() {
        if (instance == null) {
            synchronized (UserDAOImpl.class) {
                if (instance == null) {
                    instance = new TransactionDAOImpl();
                }
            }
        }
        return instance;
    }

    private InMemoryDB inMemoryDB = new InMemoryDB();

    public Transaction create(Transaction transaction) {
        Transaction transactionCreate = null;
        Connection connection = dataSource.createConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(createTransaction, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setObject(1, transaction.getProduct());
            preparedStatement.setInt(2, transaction.getProductCount());
            preparedStatement.setDouble(3, transaction.getProductPrice());
            java.sql.Date transDate = new java.sql.Date(transaction.getDate().getTime());
            preparedStatement.setDate(4, transDate);
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                transaction.setId(resultSet.getInt(1));
                transactionCreate = transaction;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return transactionCreate;
    }


    public List<Transaction> findAllByUserId(Integer id) {
        return inMemoryDB.findAllByUserId(id);
    }

    @Override
    public Transaction findTransactionById(Integer id) {
        Transaction transactionFind = null;
        Connection connection = dataSource.createConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(findTransactionById);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                transactionFind = new Transaction();
                transactionFind.setId(resultSet.getInt("id"));
                transactionFind.setProductPrice(resultSet.getDouble("productPrice"));
//                transactionFind.setProduct(resultSet.("product"));
                transactionFind.setDate(resultSet.getDate("date"));
                transactionFind.setProductCount(resultSet.getInt("productCount"));
                            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return transactionFind;
    }


    @Override
    public boolean deleteTransaction(Integer id) {
        boolean resultSet = false;
        Connection connection = dataSource.createConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(deleteTransaction);
            preparedStatement.setInt(1,id);
            resultSet = preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultSet;
    }

    @Override
    public Transaction updateTransaction(Transaction transaction) {
        Transaction transactionUpdate = null;
        Connection connection = dataSource.createConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(updateTransaction,Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1,transaction.getProductCount());
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if(resultSet.next()){
                transaction.setId(resultSet.getInt(1));
                transactionUpdate = transaction;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return transactionUpdate;
    }
}
