package com.log.market.entity;

/**
 * Created by User on 10.10.2016.
 */
public enum UserRole {
    GUEST, USER, ADMIN;

    public static UserRole findUserRoleByNumber(int index) {
        UserRole role = null;
        for (UserRole userRole : UserRole.values()) {
            if (userRole.ordinal() == index) {
                role = userRole;
            }
        }return role;
    }
}

