package com.log.market.entity;

import java.util.List;

/**
 * Created by User on 10.10.2016.
 */
public class Product {

    private Integer id;
    private String title;
    private String description;
    private String price;
    private Integer count;
    private List<ProductGroup> productGroup;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public List<ProductGroup> getProductGroup() {
        return productGroup;
    }

    public void setProductGroup(List<ProductGroup> productGroup) {
        this.productGroup = productGroup;
    }
}
