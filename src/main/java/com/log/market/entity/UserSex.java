package com.log.market.entity;

/**
 * Created by User on 10.10.2016.
 */
public enum UserSex {
    MALE, FEMALE;

    public static UserSex findUserSexByNumber(int index) {
        UserSex res = null;
        for (UserSex userSex : UserSex.values()) {
            if(userSex.ordinal() == index){
                res = userSex;
            }
        }
        return res;
    }

}

