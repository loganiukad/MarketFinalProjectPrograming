package com.log.market.holder;


import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by User on 10.11.2016.
 */
public class AppPropertiesHolder {
    private String dataBaseURL;
    private String dataBaseUser;
    private String dataBaseUserPassword;
    private String dataBaseDriver;

    private static volatile AppPropertiesHolder instance;

    private AppPropertiesHolder() {
    }

    public static AppPropertiesHolder getInstance() {
        if (instance == null) {
            synchronized (AppPropertiesHolder.class) {
                if (instance == null) {
                    instance = new AppPropertiesHolder();
                }
            }
        }
        return instance;
    }

    public void init() {
        Properties properties = new Properties();
            try {
                FileInputStream inputStream = new FileInputStream("C:\\Users\\User\\IdeaProjects\\Oracle\\src\\com\\log\\market\\resources\\application.properties");
                properties.load(inputStream);
                dataBaseUser = properties.getProperty("db.user");
                dataBaseURL = properties.getProperty("db.url");
                dataBaseUserPassword = properties.getProperty("db.password");
                dataBaseDriver = properties.getProperty("db.driver");
            } catch (IOException e) {
                e.printStackTrace();
        }
    }


    public String getDataBaseURL() {
        return dataBaseURL;
    }

    public String getDataBaseUser() {
        return dataBaseUser;
    }

    public String getDataBaseUserPassword() {
        return dataBaseUserPassword;
    }

    public String getDataBaseDriver() {
        return dataBaseDriver;
    }
}
