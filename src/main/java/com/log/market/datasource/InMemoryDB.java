package com.log.market.datasource;

import com.log.market.entity.Product;
import com.log.market.entity.ProductGroup;
import com.log.market.entity.Transaction;
import com.log.market.entity.User;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by User on 03.11.2016.
 */
public class InMemoryDB {
    private static Integer userCount = 1;
    private static Integer transCount = 1;
    private static Integer productGroupCount = 1;
    private static Integer productCount = 1;
    private Map<Integer, User> users = new HashMap<Integer,User>();
    private static Map<Integer, Transaction> transactions = new HashMap<Integer, Transaction>();
    private static Map<Integer, ProductGroup> productGroups = new HashMap<Integer, ProductGroup>();
    private static Map<Integer, Product> products = new HashMap<Integer, Product>();
    private static List<ManyToMany> manyToManyList = new LinkedList<ManyToMany>();


    public List findProductsByProductGroup(Integer id) {
        List<Integer> productList = new LinkedList();
        List<Product> resultProductList = new LinkedList();
        for (ManyToMany manyToMany : manyToManyList) {
            if (manyToMany.getProdGrId().equals(id)) {
                productList.add(manyToMany.getProdGrId());
            }
        }
        for (Integer integer : productList) {
            Product product = products.get(integer);
            resultProductList.add(product);
        }
        return resultProductList;

    }

    public List findProductGroupsByProduct(Integer id) {
        List<Integer> productGroupList = new LinkedList();
        List<ProductGroup> resultProductGroupList = new LinkedList();
        for (ManyToMany manyToMany : manyToManyList) {
            if (manyToMany.getProdId().equals(id)) {
                productGroupList.add(manyToMany.getProdId());
            }
        }
        for (Integer integer : productGroupList) {
            ProductGroup productGroup = productGroups.get(integer);
            resultProductGroupList.add(productGroup);
        }
        return resultProductGroupList;

    }

    public static class ManyToMany {
        private Integer prodId;
        private Integer prodGrId;

        public Integer getProdId() {
            return prodId;
        }

        public void setProdId(Integer prodId) {
            this.prodId = prodId;
        }

        public Integer getProdGrId() {
            return prodGrId;
        }

        public void setProdGrId(Integer prodGrId) {
            this.prodGrId = prodGrId;
        }
    }


    public synchronized boolean deleteProduct(Integer id) {
        boolean productRes = false;
        if (products.containsKey(id)) {
            products.remove(id);
            productRes = true;
        }
        return productRes;
    }


    public synchronized Product updateProduct(Product product) {
        Product productRes = null;
        if (products.containsKey(product.getId())) {
            productRes = products.put(product.getId(), product);
        }
        return productRes;
    }


    public Product findProductById(Integer id) {
        Product productRes = null;
        if (products.containsKey(id)) {
            productRes = products.get(id);
        }
        return productRes;
    }

    public synchronized Product createProduct(Product product) {
        Product productRes = null;
        if (!products.containsValue(product)) {
            products.put(productCount, product);
            productRes = product;
            List<ProductGroup> productGroup = product.getProductGroup();
            for (ProductGroup group : productGroup) {
                ManyToMany manyToMany = new ManyToMany();
                manyToMany.setProdId(product.getId());
                manyToMany.setProdGrId(group.getId());
                manyToManyList.add(manyToMany);
            }

            ++productCount;
        }
        return productRes;
    }


    public synchronized boolean deleteProductGroup(Integer id) {
        boolean productGroupRes = false;
        if (productGroups.containsKey(id)) {
            productGroups.remove(id);
            productGroupRes = true;
        }
        return productGroupRes;
    }


    public synchronized ProductGroup updateProductGroup(ProductGroup productGroup) {
        ProductGroup productGroupRes = null;
        if (productGroups.containsKey(productGroup.getId())) {
            productGroupRes = productGroups.put(productGroup.getId(), productGroup);
        }
        return productGroupRes;
    }


    public synchronized ProductGroup createProductGroup(ProductGroup productGroup) {
        ProductGroup productGroupRes = null;
        if (!productGroups.containsValue(productGroup)) {
            productGroups.put(productGroupCount, productGroup);
            productGroupRes = productGroup;
            ++productGroupCount;
        }
        return productGroupRes;
    }

    public ProductGroup findProductGroupById(Integer id) {
        ProductGroup productGroupRes = null;
        if (productGroups.containsKey(id)) {
            productGroupRes = productGroups.get(id);
        }
        return productGroupRes;
    }


    public synchronized Transaction updateTransaction(Transaction transaction) {
        Transaction transRes = null;
        if (transactions.containsKey(transaction.getId())) {
            transRes = transactions.put(transaction.getId(), transaction);
        }
        return transRes;
    }


    public synchronized boolean deleteTransaction(Integer id) {
        boolean transRes = false;
        if (transactions.containsKey(id)) {
            transactions.remove(id);
            transRes = true;
        }
        return transRes;
    }

    public List<Transaction> findAllByUserId(Integer id) {
        List<Transaction> transactionsList = new LinkedList<Transaction>();
        if (transactions.containsKey(id)) {
            transactionsList.add(transactions.get(id));
        }
        return transactionsList;
    }


    public Transaction findTransactionById(Integer id) {
        Transaction transRes = null;
        if (transactions.containsKey(id)) {
            transRes = transactions.get(id);
        }
        return transRes;

    }


    public synchronized Transaction createTransaction(Transaction transaction) {
        Transaction transRes = null;
        if (!transactions.containsValue(transaction)) {
            transactions.put(transCount, transaction);
            transRes = transaction;
            ++transCount;
        }
        return transRes;
    }

    public User findUserByLoginAndPassword(String login, String password) {
        User userRes = null;
        if (users.containsValue(login) && users.containsValue(password)) {
            userRes = users.get(users.containsValue(login));
        }
        return userRes;
    }


    public User findByLoginAndEmale(String login, String email) {
        User userRes = null;
        if (users.containsValue(users.containsValue(login)) && users.containsValue(email)) {
            userRes = users.get(users.containsValue(login));
        }
        return userRes;
    }


    public synchronized User addUser(User user) {
        User userRes = null;
        if (!users.containsValue(user)) {
            users.put(userCount, user);
            userRes = user;
            ++userCount;
        }
        return userRes;
    }


    public synchronized boolean deleteUser(Integer id) {
        boolean userRes = false;
        if (users.containsKey(id)) {
            users.remove(id);
            userRes = true;
        }
        return userRes;
    }

    public synchronized User updateUser(User user) {
        User userRes = null;
        if (users.containsKey(user.getId())) {
            userRes = users.put(user.getId(), user);
        }
        return userRes;
    }
}