package com.log.market.datasource;

import com.log.market.holder.AppPropertiesHolder;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * Created by User on 10.11.2016.
 */
public class DataSource {

    private static volatile DataSource instance;
    private static AppPropertiesHolder appPropertiesHolder;

    private DataSource() {
    }

    public static DataSource getInstance() {
        if (instance == null) {
            synchronized (DataSource.class) {
                if (instance == null) {
                    instance = new DataSource();
                    appPropertiesHolder = AppPropertiesHolder.getInstance();
                    appPropertiesHolder.init();
                }
            }
        }
        return instance;
    }

    public static Connection createConnection() {
        Connection conn = null;
        try {
            Class.forName(appPropertiesHolder.getDataBaseDriver());
            conn = DriverManager.getConnection(appPropertiesHolder.getDataBaseURL(), appPropertiesHolder.getDataBaseUser(), appPropertiesHolder.getDataBaseUserPassword());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conn;
    }
}