package com.log.market.dto;

import com.log.market.entity.Product;

import java.util.List;

/**
 * Created by User on 27.10.2016.
 */
public class ProductGroupDTO {

    private Integer id;
    private String title;
    private String description;
    private List<Product> listProductDTO;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Product> getListProductDTO() {
        return listProductDTO;
    }

    public void setListProductDTO(List<Product> listProductDTO) {
        this.listProductDTO = listProductDTO;
    }
}
